var fbLoaded = false, cycleLoaded = false, msryLoaded = false, intLess,
    windowWidth, isDesktop, isMobile, isTablet, isSmartphone;

$(document).ready(function(){
    if(APPLICATION_ENV == 'development') head.js(JS_PATH+'/less.js',function(){
        // less.watch();
        intLess = window.setInterval(function(){
            localStorage.clear();
            less.refresh();
        },3000);
    }); // load less in dev

    // ativa menus via javascript
    var i, j, menus_ativar = {
        'top-menu' : {
            '/projeto/' : 1
        }
    };
    for(j in menus_ativar){
        for(i in menus_ativar[j]){
            if(location.href.indexOf(i)!=-1){
                var $li = $('#'+j+' ul.navigation li').eq(menus_ativar[j][i]);
                $li.addClass('active').find('a').addClass('active');
            }
        }
    }

    $('#mobile-menu').click(function(e){
        $(this).toggleClass('active');
        $('#top-menu').toggleClass('show');
    });
    
    // máscaras padrões
    $("input.mask-cpf").mask("999.999.999-99");
    $("input.mask-cep").mask("99999-999");
    $("input.mask-data,input.mask-date").mask("99/99/9999");
    $("input.mask-hour").mask("99:99");
    $("input.mask-hours").mask("99:99:99");
    $("input.mask-ddd").mask("99");
    $("input.mask-tel").mask("(99)9999-9999");
    $("input.mask-cel").mask("(99)9999-9999?9");
    $("input.mask-tel-no-ddd").mask("9999-9999");
    $("input.mask-currency").maskCurrency();

    $('[autoselect]').live('focus',function(){ this.select(); });
    
    $(".prevalue").each(function(){ $(this).prevalue(); }); // prevalue em campos com a class .prevalue
    // $(".prevalue").prevalue(); // prevalue em campos com a class .prevalue
    
    $("a[href^='#']").live("click",function(e){ // remove click de links vazios (que começam por #)
        if(!$(this).hasClass('scroll-to')) e.preventDefault();
    });
    $("a.js-back").live("click",function(e){ // adiciona ação de voltar a links com a classe js-back
        history.back();
    });
    
    if($(".pagination").size()){ // se tiver paginação
        $(".pagination .navigation.left").html("&laquo;").attr('title','Página anterior'); // arruma texto da navegação
        $(".pagination .navigation.right").html("&raquo;").attr('title','Próxima página');
        if($(".pagination a").size() <= 1){
            $(".pagination").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }
    
    if($(".paginacao").size()){ // se tiver paginação
        if($(".paginacao a").size() < 1){
            $(".paginacao").hide(); // se tiver somente 1 ou nenhuma página esconde a div
        }
    }
    
    if(ENV_DEV) head.js(JS_PATH+'/tests.js'); // carrega scripts de páginas
    if(SCRIPT) head.js(JS_PATH+'/'+SCRIPT+'.js'); // carrega scripts de páginas
    if(CONTROLLER!='contato') head.js(JS_PATH+'/contato.js'); // carrega scripts de páginas

    // newsletter
    $('.newsletter-form').submit(function(e){
        e.preventDefault();
        var $form = $(this), $status = $('.newsletter-form .status').removeClass('error').html('Enviando...');
        
        $.post(URL+'/index/newsletter.json',$form.serialize(),function(json){
            if(json.error) $status.addClass('error');
            $status.html(json.message);
        },'json');
    });
    
    // busca
    var b = $("#busca");
    $('#frm-busca').submit(function(e){
        e.preventDefault();
        
        if(b.val() == '' || b.val() == b.data('prevalue') || b.val() == b.attr('placeholder')){
            alert2('Escreva algum produto para buscar');
            b.focus();
        } else {
            window.location = URL+'/busca/'+b.val();
        }
        
        return false;
    });
    
    // flash-messages
    if($("#flash-messages").size() && CONTROLLER!='admin'){
        var $flash = $("#flash-messages");
        var y = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : document.body.scrollTop;
        $flash.css({top:y+10}).delay(1000).show().live('click',function(){
            $(this).fadeOut("slow");
        });
    }
    
    // scroll to
    var $scrollTos = $('a.scroll-to');
    if($scrollTos.length){
        head.js(JS_PATH+'/jquery.scrollTo-min.js',function(){
            // $scrollTos.live('click',function(e){
            $scrollTos.click(function(e){
                var $this = $(this);
                console.log($this.attr('href'));
                $.scrollTo($($this.attr('href')),1000);
                // $.scrollTo($($this.attr('href')),1000,{axis:'y'});
                // $.scrollTo($this.attr('href'),1000,{axis:'y'});
            });
        });
    }

    // accordion
    if(Accordion.wrapper.size()) Accordion.init();

    // ações por páginas
    switch(CONTROLLER){
        case 'index':
            // banner
            _loadCycle(function(){
                $('#slideshow').before('<ul id="banner-nav">').cycle({
                // $('#slideshow').cycle({
                    fx:'fade',
                    // speedIn:4000,
                    // speedOut:100,
                    pager:"#banner-nav",
                    speed:(APP_ENV=='development')?2000:2000
                });
            });
            break;
        case 'projeto':
            if(ACTION!='index') {
                _loadFancybox(function(){
                    $('.projeto').fancybox({
                        padding: 0,
                        centerOnScroll: true
                    });
                });
                // _loadMasonry(function(){
                //     var grid = $('.grid').masonry({
                //         itemSelector: '.grid-item',
                //         columnWidth: '.grid-sizer',
                //         percentPosition: true
                //     });
                //     window.setTimeout(function(){
                //         grid.masonry('layout');
                //     },500);
                // });
            }
            break;
        case 'mobiliario-exclusivo':
            if(ACTION!='index1') {
                _loadFancybox(function(){
                    var fbOpts = {
                        titlePosition: 'inside',
                        titleFormat: formatTitleMobiliario,
                        padding: 0,
                        centerOnScroll: true
                    };

                    if(isSmartphone) {
                        fbOpts.centerOnScroll = false;
                        // fbOpts.margin = 0;
                        // fbOpts.width = '95%';
                        // fbOpts.height = '95%';
                        // fbOpts.autoScale = true;
                        // fbOpts.autoDimensions = true;
                    }

                    $('.projeto').fancybox(fbOpts);
                });
            }
            break;
        case 'sobre':
            if(ACTION!='index1') {
                _loadFancybox(function(){
                    $('.impresso').fancybox({
                        padding: 0,
                        centerOnScroll: true
                    });
                });
            }
            break;
        case 'projetos':
            // head.js(JS_PATH+'/jquery.tmpl.min.js');
            head.js(JS_PATH+'/jquery.tmpl.min.js',function(){
                $.getJSON(URL+'/index/posts-instagram.json?count=2',function(json){
                    for(var j in json)
                    $('#posts-instagram')
                        .tmpl({
                            url:'https://instagram.com/p/'+json[j].code,
                            caption:json[j].caption,
                            img:json[j].thumbnail_src
                        })
                        .appendTo('.social-embed.instagram');
                });
                $.getJSON(URL+'/index/posts-pinterest.json?count=2',function(json){
                    for(var j in json)
                    $('#posts-pinterest')
                        .tmpl({
                            url:json[j].board_link,
                            board_image:json[j].board_image,
                            board_name:json[j].board_name,
                            board_pin_count:json[j].board_pin_count
                        })
                        .appendTo('.social-embed.pinterest');
                });
            });
            break;
    }

    // checando features
    if(!head.placeholder){
        var $elm;

        $('[placeholder]').each(function(){
            $elm = $(this);

            $elm.addClass('prevalue')
                .data('prevalue',$elm.attr('placeholder'))
                .prevalue(); 
        });
    }
    
    if(!head.number){
        var $elm;

        $('input[type=number]').each(function(){
            $elm = $(this);

            $elm.addClass('prevalue').addClass('mask-int')
                .data('prevalue',$elm.attr('placeholder'))
                .prevalue();

            _onlyNumbers('.mask-int');
        });
    }

    // old ie
    if($.browser.msie && $.browser.version < 9){
        // old ie hacks
    }

    $('.input-number').live('blur',function(){
        var $this = $(this),
            val = Number($this.val());

        if($this.attr('max')) {
            var max = Number($this.attr('max'));
            
            if(max < val) {
                alert2('O valor está acima do permitido ('+max+')');
                window.setTimeout(function(){$this.focus();},100);
            }
        }

        if($this.attr('min')) {
            var min = Number($this.attr('min'));

            if(min > val) {
                alert2('O valor está abaixo do permitido ('+min+')');
                window.setTimeout(function(){$this.focus();},100);
            }
        }
    });

    checkMobile();
    $(window).resize(function(e){
        checkMobile();
    });
});

// Color Hex <-> RGB
function hexToR(h){return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h){return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h){return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h){return (h.charAt(0)=="#") ? h.substring(1,7):h}
function hexToRGB(h){return {'R':hexToR(h),'G':hexToG(h),'B':hexToB(h)}}
function RGBToHex(r,g,b){var dec = r+256*g+65536*b;return dec.toString(16);}

// Math
function getRand(n){n=n||0;return Math.floor(Math.random()*(n+1));}

// ValidateForm
function validadeForm(f){var $f=$(f),err=0;$f.find("input").each(function(i,v){if($(this).data("validate")&&$.trim($(this).val())==""){alert2($(this).data("errmsg"));$(this).focus();err++;return false}});if(err==0){$f.submit()}};

// Currency
function currency(val){return 'R$ '+(val+'').replace('.',',');}

// valida campo vazio
function isEmpty(elm) {
    var v = $.trim(elm.val());

    return v=='' || v==elm.data('prevalue') || v=='__none__';
}

// helper console.log
function _log(log) {
    if(typeof(window.console)=='undefined'){
        return alert2(log);
    }

    return console.log(log);
}
function _d(log){ _log(log); } // alias para _log()

// 9 digitos
function digito9(elm,options){
    var ddd9digit = ['11','12','13','14','15','16','17','18','19'], // ddd's que precisam de 9 dig
        tp9digit  = ['cel'], // tipos que precisam de 9 dig
        dddEmpty = ['','__','ddd: *','ddd','ddd:'],
        $this = $(elm),
        opts = {
            'premask' : false,
            'ddd'     : 'input.mask-ddd',
            'tipotel' : 'select.tipotel'
        },
        opts2 = options || {};
        

    var opt = $.extend(opts,opts2);
    
    var dddField      = $(opt.ddd),
        ddd           = dddField.val(),
        tipotelField  = opt.tipotel ? $(opt.tipotel) : null,
        tipotel       = tipotelField ? tipotelField.val() : 'cel';
    
    // função que checa se precisa adicionar o dig
    var has9digit = function(){
        if($.inArray(dddField.val(),ddd9digit)!=-1){
            if(tipotelField){
                if($.inArray(tipotelField.val(),tp9digit)!=-1){
                    return true;
                }
            } else {
                if($.inArray(tipotel,tp9digit)!=-1){
                    return true;
                }
            }
        }
            
        return false;
    };

    if(opt.premask) $this.mask(has9digit()?'99999-9999':'9999-9999');

    $this.live('focus',function(){
            // console.log([$.trim(dddField.val().toLowerCase()),opt.tipotel]);
            $this.unmask();

            if($.inArray($.trim(dddField.val().toLowerCase()),dddEmpty)!=-1) {
                    $this.blur();
                    dddField.focus();
                    alert2('Digite seu DDD');
                    return false;
            }

            $this.mask(has9digit()?'99999-9999':'9999-9999');
    });
}

/* onlyNumbers */
function _onlyNumbers(selector){
    $(selector).keypress(function(e){
        return (e.which >= 48 && e.which <= 57) || ($.inArray(e.which,[0,13,8]) != -1);
    });
}

// head.js features
head.feature('placeholder', function() {
    var t = document.createElement('textarea'); // #=> <textarea></textarea>
    return (t.placeholder !== undefined); // #=> true || false
});

head.feature('number', function() {
    var t = document.createElement('input'); 
    // if(!$.browser.msie) t.type = 'number'; // #=> <input type="number">
    return (t.max !== undefined); // #=> true || false
});

// loaders
function _loadFancybox(callback){
    if(fbLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        $.getCss(CSS_PATH+'/fancybox/jquery.fancybox-1.3.4.css');
        head.js(JS_PATH+'/jquery.fancybox-1.3.4.pack.js',function(){
            fbLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _loadMasonry(callback){
    if(msryLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        // head.js(JS_PATH+'/jquery.masonry.min.js',function(){
        head.js(JS_PATH+'/masonry.pkgd.min.js',function(){
            msryLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

function _loadCycle(callback){
    if(cycleLoaded){
        if(typeof(callback)=='function') (callback)();
    } else {
        head.js(JS_PATH+'/cycle.min.js',function(){
            cycleLoaded = true;
            if(typeof(callback)=='function') (callback)();
        });
    }
}

var Accordion = {
    wrapper: $('.accordion-wrapper'),

    open: function(elm){
        elm.addClass('open').addClass('active');
        $(elm.attr('href')).slideDown();
    },

    close: function(elm){
        elm.removeClass('open').removeClass('active');
        $(elm.attr('href')).slideUp();
    },

    closeAll: function(){
        if(this.wrapper.hasClass('accordion-keep-opens')) return;
        $('.accordion-item',this.wrapper).removeClass('open').removeClass('active');
        $('.accordion-content',this.wrapper).slideUp();
    },

    init: function(){
        var _acc = this;
        
        this.wrapper.find('.accordion-item').each(function(i,elm){
            var $elm = $(elm);
            $elm.click(function(e){
                e.preventDefault();
                
                if($elm.hasClass('open')||$elm.hasClass('active'))
                    return _acc.close($elm);

                _acc.closeAll();
                _acc.open($elm);
            });
        });
    }
}

function formatTitleMobiliario(title, currentArray, currentIndex, currentOpts) {
    // console.log(title, currentArray, currentIndex, currentOpts);
    var elm = $(currentArray[currentIndex]);
    return '<div id="fb-description">'+
        // '<span><a href="javascript:;" onclick="$.fancybox.close();"></a></span>' + 
        '<span class="titulo">'+(elm.data('title'))+'</span>' + 
        '<span class="descricao">'+(elm.data('description'))+'</span>' + 
        '<span class="img-count">' + (currentIndex + 1) + ' de ' + currentArray.length + '</span>' +
        '</div>';
}


// mobile
function checkMobile(){
    windowWidth = $(window).width(); // window.innerWidth
    isDesktop = windowWidth >= 1200;
    isMobile = windowWidth < 1200;
    isTablet = windowWidth < 1200 && windowWidth >= 768;
    isSmartphone = windowWidth < 767;

    var $html = $('html');

    var log = windowWidth;

    if(isSmartphone){
        if(CONTROLLER=='index') {
            var w = ($('#content.content-index #stands-list .stand').size() * 200) + 20;
            $('#content.content-index #stands-list').css('width',w+'px');
        }
    }
    if(isMobile){ }
    if(isTablet){ }
    if(isDesktop){ }

    // if(APP_ENV=='development') console.log(log);
}