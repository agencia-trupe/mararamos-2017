$.fn.extend({
    prevalue: function(v,type){
        v = v || $(this).data('prevalue');
        type = type || $(this).attr("type");
        if(!type || type == "text" || type == "textarea" || type === undefined){
            $(this).addClass("pre").val(v).focus(function(){
                if(this.value == v){
                    $(this).removeClass("pre").val("");
                }
            }).blur(function(){
                if(this.value.length == 0){
                    $(this).addClass("pre").val(v);
                }
            });
        } else if(type == "password"){
            var $model = $(this).clone();
            var real_id = $model.attr("id");
            var $new = "<input type='text' "
                      +"name='"+$model.attr("name")+"_clone'"
                      +"id='"+$model.attr("id")+"_clone'"
                      +"class='"+$model.attr("class")+"'"
                      +"/>";
            $(this).css("display","none");
            $new = $($new).insertBefore(this).addClass("pre").val(v).focus(function(){
                $(this).css("display","none");
                $("#"+real_id).show().focus();
            });
            $("#"+real_id).blur(function(){
                if(this.value.length == 0){
                    $(this).css("display","none");
                    $("#"+real_id+"_clone").show();
                }
            });
        }
        return $(this);
    }
});