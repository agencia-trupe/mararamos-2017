Data = {
    am2br: function(dt){
        return dt.split('-').reverse().join('/');
    },
    br2am: function(dt){
        return dt.split('/').reverse().join('-');
    }
}