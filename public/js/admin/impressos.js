var MAX_FOTOS = 12, MAX_SIZE = 5242880, allow_photos=false;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,"+
                "separator01,createLink,separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px','height':'170px'});
    $('div.wysiwyg').css({'margin-left':'110px','height':'200px'});
    $('div.wysiwyg div').remove();
    
    $("#frm-impressos").submit(function(e){
        if($.trim($("#titulo").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-impressos").submit(e);
    });
    
    $("#search-by").change(function(){
        if($(this).val() == "categoria_id"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(categorias){
                for(i in categorias){
                    $combo.append('<option value="'+i+'">'+categorias[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($(this).val() == "role"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(roles){
                for(i in roles){
                    $combo.append('<option value="'+i+'">'+roles[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        // } else if($("#search-txt").attr("type").indexOf('select') != -1){
        } else if(document.getElementById('search-txt').type.indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });
    
    // arruma css de radios no IE
    if($.browser.msie){
        $('li.radio label:nth-child(2),li.radio label:nth-child(3)').css({'text-align':'left','width':'75px'});
    }
    
    // seta cor para a foto
    $('.list_fotos select').live('change',function(){
        var $this = $(this),
            $li   = $this.parents('li'),
            foto_id = $li.find('.foto_id').val(),
            impresso_id = $('#impressos_id').val(),
            data = {'foto':foto_id,'impresso':impresso_id,'cor':$this.val()},
            url  = [URL,CONTROLLER,DIR,'foto-cor.json'].join('/');
        
        if(data.cor == 0) {
            alert('Escolha uma cor');
            return false;
        }
        
        $this.attr('disabled',true);
        
        $.post(url,data,function(json){
            $this.attr('disabled',false);
            if(json.error) alert(json.error);
        },'json');
    });

    // valida form
    $('#frm-impressos').submit(function(e){
        var valids = {
                /*'Number($.trim($("#peso").val()).replace(",","."))>0' : {
                    'focusIn':'#peso',
                    'message':'Preencha o peso do impresso para que seja possível calcular o frete'
                }*/
            },
            invalid = isInvalid(valids);
        
        if(invalid){
            e.preventDefault();
            alert(invalid);
            return false;
        }
    });

    // add / del categorias
    Categoria.checkButtons();
    $('.cat_field select').attr('name',$('.cat_field select').attr('name')+'[]');

    $('.add_cat').live('click',function(e){
        Categoria.add($(this));
    });
    $('.del_cat').live('click',function(e){
        Categoria.del($(this));
    });
});

function isInvalid(a){
    for(i in a){
        if(!eval(i)){
            if(typeof(a[i])=='object') {
                $(a[i]['focusIn']).focus();
                return a[i]['message'];
            } else {
                return a[i];
            }
        }
    }

    return false;
}

var Categoria = {
    add : function(elm){
        var cat_field = elm.parent('.cat_field'),
            new_cat_field = cat_field.clone(),
            id = $('.cat_field').size();

        new_cat_field.attr('id','cat_field_'+id).addClass('dup');
        new_cat_field.find('select').attr('id','categoria_id_'+id).addClass('dup');
        new_cat_field.insertAfter(cat_field);
        $('.del_cat').show();
    },

    del : function(elm){
        if(confirm('Deseja removar esta categoria do impresso?')){
            elm.parent('.cat_field').fadeOut(function(){
                elm.parent('.cat_field').remove();
                Categoria.checkButtons();
            });
        }
    },

    checkButtons : function(){
        var cat_fields = $('.cat_field');

        if(cat_fields.length == 1){
            cat_fields.find('.del_cat').hide();
        }
    }
};