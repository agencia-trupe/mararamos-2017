Messenger = {
    elm       : "#flash-messages",
    msgs      : new Array(),
    container : $(document),
    moreCount : 1,
    moreClass : 'more',
    
    add: function(msg,type){
        this.msgs.push({"type":type||"message","message":msg});
        return this;
    },
    
    show: function(dlay,opt){
        var opts = {
            cleanBefore : true,
            showType    : "prepend"
        }
        
        //if($(this.elm).size() == 0){
        //    var elm = this.showType=="prepend" ?
        //          $(document.createElement('div')).attr("id","flash-messages").prependTo(this.container) :
        //          $(document.createElement('div')).attr("id","flash-messages").appendTo(this.container);
        //    
        //    $("<ul></ul>").appendTo(elm);
        //} else {
        //    var elm = $(this.elm);
        //}
        var elm  = $(this.elm),
            item = elm.find("ul"),
            o    = opt ? $.extend(opts,opt) : opts;
        
        elm.hide();
        
        if(o.cleanBefore){
            item.children("li").remove();
            item.html("");
        }
        
        for(var i = 0,msg_size = this.msgs.length;i<msg_size;i++){
            //alert(this.msgs[i].message);
            elm.addClass(this.msgs[i].type);
            item.append("<li class='"+this.msgs[i].type+"'>"+this.msgs[i].message+"</li>");
        }
        
        if(this.count() > this.moreCount){
            elm.addClass(this.moreClass);
        }
        
        if(elm.is(":hidden")){
            if(dlay){
                elm.fadeIn("slow").delay(dlay).fadeOut("slow");
            } else {
                elm.fadeIn("slow").delay(dlay).fadeOut("slow");
            }
        }
        
        if(o.cleanBefore){
            this.msgs = new Array();
        }
    },
    
    count: function(){
        return this.msgs.length;
    },
    
    reset: function(){
        this.msgs = new Array();
        $(this.elm).find("ul").html("");
    }
}