var Msg = Messenger;
Msg.container = $("div.main .content");

$(document).ready(function(){
    //head.js(JS_PATH+'/less.js');
    
    $("a[href^='#']").click(function(e){
        e.preventDefault();
    });
	
	$('input[type=checkbox],input[type=radio]').css('border','none');
    
    var menu_time = "fast";
    $(".admin-menu .navigation ul").addClass("submenu");
    $(".admin-menu .navigation ul ul").removeClass("submenu").addClass("submenusub");
    
    $(".admin-menu a.has-sub").append(' &darr;');
    $(".admin-menu a.has-sub").mouseenter(function(){
        clearTimeout($(this).data('timeoutId'));
        $(this).parent().find("ul.submenu").slideDown(menu_time);
    }).mouseleave(function(){
        var someelement = this;
        var timeoutId = setTimeout(function(){ $(someelement).parent().find("ul.submenu").slideUp(menu_time);}, 200);
        $(someelement).data('timeoutId', timeoutId);
    });
    $("ul.submenu").mouseenter(function(){
        clearTimeout($(this).parent().find("a.has-sub").data('timeoutId'));
    }).mouseleave(function(){
        $(this).slideUp(menu_time);
    });
    $('ul.submenu').each(function(){
        var $this = $(this);
        $this.attr('id','sub'+$this.prev('a').attr('id'));
    });

    // $(".admin-menu a.has-sub-sub").append(' &rarr;');
    // $(".admin-menu a.has-sub-sub").mouseenter(function(){
    $(".admin-menu > ul > li ul li:has(ul.submenusub)").find('a.has-sub-sub').append(' &rarr;').mouseenter(function(){
        clearTimeout($(this).data('timeoutId'));
        $(this).parent().find("ul.submenusub").fadeIn(menu_time);
    }).mouseleave(function(){
        var someelement = this;
        var timeoutId = setTimeout(function(){ $(someelement).parent().find("ul.submenusub").fadeOut(menu_time);}, 200);
        $(someelement).data('timeoutId', timeoutId);
    });
    $("ul.submenusub").mouseenter(function(){
        clearTimeout($(this).parent().find("a.has-sub-sub").data('timeoutId'));
    }).mouseleave(function(){
        $(this).slideUp(menu_time);
    });
    
    $("input.txt,select.txt,textarea.txt").live('focus',function(){
        $(this).addClass('hover');
    }).live('blur',function(){
        $(this).removeClass('hover');
    });
    
    if(CONTROLLER!='categorias')
        $(".row-list li").live("mouseover",function(){
    		$(this).find("a.row-action").show();
    	}).live("mouseout",function(){
    		$(this).find("a.row-action").hide();
    	});
	
	_onlyNumbers('.mask-int');

    // ordenação
    var $tableOrder = $('.search-result table .row-order');
    if($tableOrder.size() && !IS_BUSCA) ajust_order();
    
    if($tableOrder.size()) $tableOrder.live('click',function(){
        if(!$(this).hasClass('disabled')){
            var row = $(this).parent('td').parent('tr');
            if($(this).hasClass('order-down')){
                row.insertAfter(row.next());
            }else{
                if(row.prev().attr("class") != "title"){
                    row.insertBefore(row.prev());
                }
            }
            ajust_order();
        }
    });
    
    if($tableOrder.size()) $('.search-result table tbody').sortable({
        items: "tr",
        revert:true,
        start:function(e,ui){
            $('.search-result table tbody tr').addClass('disabled');
            ui.item.removeClass('disabled').addClass('grabber');
        },
        stop:function(e,ui){
            $('.search-result table tbody tr').removeClass('disabled').removeClass('grabber');
            ajust_order();
        }
    });
});

/* Alias */
(function($){  
    $.fn.getAlias = function(from) {
        var t = $(this),
            f = $(from);
        
        f.bind('keyup',function(){
            t.val('/'+normalize(f.val())+'/');
        });
        
        var normalize = function(str){
            return str.toLowerCase().replace(/[^a-z0-9]+/g,'-');
        }
    }
})(jQuery);

function _contentImgSize(){
    // console.log('img');
    $('div.wysiwyg,div.wysiwyg iframe').contents().find('img').css({'max-width':'98%'});
}

// ajusta ordem da lista e habilita/desabilita botões de ordenação
function ajust_order(){
    var values = {'id':[],'ordem':[]};

    $('.search-result table .row-ordem').each(function(i,v){
        var $this = $(this);
        $this.val(i+1);

        values.id.push($this.parent('td').find('.row-id').val());
        values.ordem.push(i+1);
    });

    $.post(URL+'/admin/'+CONTROLLER+'/ordem.json',values,function(json){
        if(json.error) alert(json.error);
    },'json');
    
    $('.search-result table .row-order').removeClass('disabled');
    $('.search-result table .order-up:first,.search-result table .order-down:last').addClass('disabled');
}