var MAX_FOTOS = 1, MAX_SIZE = 5242880, allow_photos;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    
    $("#frm-"+DIR).submit(function(e){
        if($.trim($("#titulo").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    var divWysiwygHeight = $('div.wysiwyg').height();

    /*if($('#link').hasClass('vis')){
        $('#link').parents('li').find('label').find('input').attr('checked',true);
        $('div.wysiwyg').css('height','5px');
    }
    if($('#body').hasClass('vis')){
        $('#body').parents('li').find('label').find('input').attr('checked',true);
        $('div.wysiwyg').addClass('vis');
    } else {
        $('div.wysiwyg').css('height','5px');
    }

    $('.radios').live('change',function(){
        var $this = $(this);
        
        if($this.hasClass('link')){
            $('#link').addClass('vis');
            $('#body').removeClass('vis');
            $('div.wysiwyg').removeClass('vis').css('height','5px');
        }

        if($this.hasClass('body')){
            // console.log($('div.wysiwyg').length);
            $('#body').addClass('vis');
            $('div.wysiwyg').addClass('vis').css('height',divWysiwygHeight);
            $('#link').removeClass('vis');
        }
    });*/
});