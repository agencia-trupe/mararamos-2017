var Test = {};

Test.form = function(form,values,autosubmit){
	var autosub = autosubmit || false, v;
	for(v in values) $(v,form).val(values[v]);
	if(autosub) $(form).submit();
}

Test.formTrabalhe = function(){
	_sto(function(){
		$('#trabalhe-wrapper').addClass('open');
		$('a[href="#frm-trabalhe-campos-3"]').trigger('click');
		$('.sexo-0').trigger('click');
		$('#escolaridade option').get(-1).selected = true;
	},.1);
	Test.form('form.trabalhe',{
		'#nome': 'Teste',
		'#email': 'teste'+SITE_NAME+'@mailinator.com',
		'#telefone': '(11)10101-1010',
		'#data_nasc': '01/01/1980',
		'#cidade': 'São Paulo',
		'#estado': 'SP',
		'#cursos': 'cursos lorem ipsum',
		'#idiomas': 'idiomas lorem ipsum',
		'#area_interesse': 'areas lorem ipsum',
		'#pretensao': 'R$ 1.000,00',
		'#empresa': 'empresa lorem ipsum',
		'#empresa_cargo': 'cargo lorem ipsum',
		'#empresa_data_inicio': '01/01/2016',
		'#empresa_data_saida': '01/01/2017',
		'#empresa2': 'empresa lorem ipsum',
		'#empresa2_cargo': 'cargo lorem ipsum',
		'#empresa2_data_inicio': '01/01/2016',
		'#empresa2_data_saida': '01/01/2017',
	});
}

function _sto(fn,t){window.setTimeout(fn,(t||1)*1000);}
function _test(c,a,s){(Test[c||CONTROLLER][a||ACTION])();}
// if(CONTROLLER=='reservas' && ACTION=='cadastro') _test();
// if(MODULE!='admin') _test();