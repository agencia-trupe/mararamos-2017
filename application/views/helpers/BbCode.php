<?php
/**
 * Aplicação de bbCodes simples para [video],[audio]
 * Auxiliar da Camada de Visualização
 * @author Patrick M. de Olveira - Trupe Agência Criativa
 * @see APPLICATION_PATH/views/helpers/BbCode.php
 */

class Zend_View_Helper_BbCode extends Zend_View_Helper_Abstract
{
    /**
     * Método Principal
     *
     * @param string $text - Texto a ser aplicado o bbCode
     *
     * @return string - Texto com bbCode renderizado
     */
    public function bbCode($text)
    {
        $_s = array(); $_r = array();

        $pr = "|\[video\](.[^\[]*)\[\/video\]|";
        preg_match_all($pr,$text,$out);
        // _d($out);

        if((bool)$out[0]){
            $p = '\[video\](.*)\[\/video]';
            
            for($i=0;$i<sizeof($out[0]);$i++){
                $pu = strstr($out[1][$i],'youtube.com') ? 
                    Youtube::getCode($out[1][$i]) : 
                    Vimeo::getCode($out[1][$i]);
                // $embedUrl = 'http://www.youtube.com/embed/'.$pu.'?autohide=1';
                $embedUrl = strstr($out[1][$i],'youtube.com') ? 
                    Youtube::embedUrl($pu,'autohide=1'):
                    Vimeo::embedUrl($pu);
                $_s[] = $out[0][$i];
                $_r[] = '<div class="video-container">'.
                // $_r[] = '<div class="video">'.
                        '<iframe width="100%" height="100%" scrolling="no" frameborder="no" '.
                        'src="'.$embedUrl.'"></iframe>'.
                        '</div>';
            }

            $text = str_replace($_s, $_r, $text);
        }

        return $text;
    }
}