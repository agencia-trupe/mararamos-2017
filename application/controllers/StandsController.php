<?php

class StandsController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'table' => 'Produtos',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // $rows = $this->table->fetchAllWithPhoto(
        // 	'status_id=1', 't1.titulo,t2.ordem',
        // 	null, null, array(
        // 		'group' => 't1.id',
        // 		'extra_fields' => 'min(t2.ordem) as foto_ordem',
        // 	)
        // );
        $rows = _utfRows($this->table->fetchAll('status_id=1','titulo'));
        $rows = $this->table->getFotos($rows,1);
        $this->view->rows = $rows;
    }

    public function internaAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('stands');
    	
    	$row = $this->table->getWithFotos($alias);
    	if(!$row) return $this->_redirect('stands');
    	
    	$this->view->row = $row;
    }


}

