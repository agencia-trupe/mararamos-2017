<?php

class MobiliarioExclusivoController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'table' => 'Produtos',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$tipo = 3;
    	$where = 'status_id=1 ';
        $where.= 'and tipo = "'.$tipo.'" ';
        $rows = _utfRows($this->table->fetchAll($where,'titulo'));
        $rows = $this->table->getFotos($rows,1);
        $this->view->rows = $rows;
        $this->view->titulo = $this->table->getTipos($tipo);
    }

}

