<?php

class SobreController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'table' => 'Paginas',
		'webs' => 'Webs',
		'impressos' => 'Impressos',
		'videos' => 'Videos',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $pagina = _utfRow($this->table->get(2));
        $this->view->pagina = $pagina;
        
        // $pagina2 = _utfRow($this->table->get(6));
        $pagina2 = $this->table->getPagina(6,1,0,0);
        $this->view->pagina2 = $pagina2;

        $webs = $this->webs->fetchAllWithPhoto('status_id=1','ordem');
        $this->view->webs = $webs;

        $impressos = $this->impressos->fetchAllWithPhoto('status_id=1','ordem',null,null,array(
        	'group' => 't1.id',
        ));
        $impressos = $this->impressos->getFotos($impressos,1);
        $this->view->impressos = $impressos;

        $videos = _utfRows($this->videos->fetchAll('status_id=1','ordem'));
        $this->view->videos = $videos;
    }


}

