<?php

class ProjetosController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'table' => 'Produtos',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
    	$where = 'status_id=1 ';
    	if($this->_hasParam('alias')) {
    		$alias = $this->_getParam('alias');
    		$tipo = $this->table->getTipoByAlias($alias);
        } else {
            $tipo = 1;
        }
        $where.= 'and tipo = "'.$tipo.'" ';
        $rows = _utfRows($this->table->fetchAll($where,'ordem'));
        $rows = $this->table->getFotos($rows,1);

        $this->view->rows = $rows;
        $this->view->titulo = $this->table->getTipos($tipo);
    }

    public function internaAction()
    {
    	$alias = ($this->_hasParam('alias')) ? $this->_getParam('alias') : null;
    	if(!$alias) return $this->_redirect('projetos');
    	
    	$row = $this->table->getWithFotos($alias);
    	if(!$row) return $this->_redirect('projetos');

        $fotos1 = array(); $fotos2 = array();
        $i=-1; foreach($row->fotos as $foto){ $i++;
            if($i%2==0) $fotos1[] = $foto;
            else $fotos2[] = $foto;
        }
        
        $this->view->fotos1 = $fotos1;
        $this->view->fotos2 = $fotos2;
    	$this->view->row = $row;
        $this->view->titulo = $this->table->getTipos($row->tipo);
    }


}

