<?php

class IndexController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'paginas' => 'Paginas',
		'destaques' => 'Destaques',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $pag = _utfRow($this->paginas->get(1));
        $this->view->pagina = $pag;

        $banners = $this->destaques->fetchAllWithPhoto('status_id=1','ordem',10);
        $this->view->banners = $banners;
    }

    public function mailAction()
    {
        try{
            Trupe_MaraRamos_Mail::send(SITE_NAME.'@mailinator.com','Trupe','Confirmacao','Confirmar leitura do texto');
            echo "OK";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }

    public function postsInstagramAction()
    {
        $count = ($this->_hasParam('count')) ? $this->_getParam('count') : 2;
        return posts_instagram($this->view->de->instagram,$count);
    }

    public function postsPinterestAction()
    {
        $count = ($this->_hasParam('count')) ? $this->_getParam('count') : 2;
        return posts_pinterest($this->view->de->pinterest,$count);
    }


}