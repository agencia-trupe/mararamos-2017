<?php

class ContatoController extends ZendPlugin_Controller_Action
{
    protected $mensagem_id1 = 11;
    protected $mensagem_id2 = 12;

    protected $_require_db = array(
        'paginas' => 'Paginas',
    );

    public function requires()
    {
        $this->mailling = new Application_Model_Db_Mailling();
        $this->mensagens = new Application_Model_Db_Mensagens();
        $this->mensagens_contato = new Application_Model_Db_MensagensContato();
        $this->arquivos = new Application_Model_Db_Arquivos();
        
        $this->mensagem1 = $this->mensagens->get($this->mensagem_id1);
        $this->mensagem2 = $this->mensagens->get($this->mensagem_id2);
    }


    public function init()
    {
        /* Initialize action controller here */
        $this->view->meta_description = 'Contato '.SITE_TITLE;
        $this->messenger = new Helper_Messenger();

        $this->view->section = $this->section = "contato";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/".$this->section;
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/..".IMG_PATH."/".$this->section;
        // $this->file_path = $this->view->file_path = APPLICATION_PATH."/..".FILE_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."".FILE_PATH."/".$this->section;

        $this->view->mensagem_id1 = $this->mensagem_id1;
        $this->view->mensagem_id2 = $this->mensagem_id2;

    }

    public function indexAction()
    {
        $this->view->titulo = 'Contato';

        $pagina = $this->paginas->getPagina(4);
        $this->view->pagina = $pagina;

        $form1 = new Application_Form_Contato($this->mensagem_id1);
        $this->view->form = $form1;
        $this->view->form1 = $form1;
        // if($this->_hasParam('dump')) _d($this->view->controller);
    }
    
    public function trabalheConoscoAction()
    {
        $this->view->titulo = 'TRABALHE CONOSCO';

        $form2 = new Application_Form_Contato($this->mensagem_id2);
        $this->view->form = $form2;
        $this->view->form2 = $form2;
    }
    
    public function enviarAction()
    {
        $this->requires();
        $r = $this->getRequest();
        if(!$r->isPost()) return $this->postMessage('Requisição inválida','error');
        $file = null; $rename = null;
        
        $post = $r->getPost();
        $assunto = (bool)trim($r->getParam('assunto')) ? trim($r->getParam('assunto')) : '';
        $form = new Application_Form_Contato($post['mensagem_id']);
        $email_assunto = null;
        $post_mensagem_id = $post['mensagem_id'];
        if($post['mensagem_id']==11){
            $assuntos = $form->getElement('assunto')->getMultiOptions();
            // $post['assunto'] = $assuntos[$post['assunto']];
            $assunto = $assuntos[$post['assunto']];
            // $email_assunto = $post['assunto'];
            // $post['mensagem_id'] = $post['assunto'];
        }
        if($post['mensagem_id']==12){
            $this->_url = 'contato/trabalhe-conosco';
        }

        $this->mensagem = $this->mensagens->get($post['mensagem_id']);
        $redirect_url = isset($post['redirect_url']) ? $post['redirect_url'] : $this->_url;

        if($form->isValid($post)){ // valida post
            // salvar em mensagens_contato
            switch ($post_mensagem_id) {
                case 11: // fale conosco
                    $html = "<h1>Contato ".$assunto."</h1>". // monta html
                        // "<b>Assunto:</b> ".$assunto."<br/><br/>".
                        nl2br($r->getParam('mensagem'))."<br/><br/>".
                        "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                        "<b>E-mail:</b> <a href='mailto:".
                        $r->getParam('email')."'>".$r->getParam('email').
                        "</a><br/>".
                        "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>";
                        // "<b>Empresa:</b> ".$r->getParam('empresa');
                    
                    $data_mensagem_contato = array(
                        'nome'     => $post['nome'],
                        // 'empresa'  => $post['empresa'],
                        'email'    => $post['email'],
                        'telefone' => Is_Cpf::clean($post['telefone']),
                        // 'cpf'      => Is_Cpf::clean($post['cpf']),
                        'assunto'  => $assunto,
                        'mensagem' => $post['mensagem'],
                        'data_cad' => date('Y-m-d H:i:s'),
                        'mensagem_id' => $post['mensagem_id'],
                    );
                    break;
                
                case 12: // trabalhe conosco
                    // upload do curriculo
                    if((bool)@$_FILES['curriculo']) if((bool)@$_FILES['curriculo']['size']){
                        $file = $_FILES['curriculo'];

                        $v = array( // validações
                            'ext' => 'doc,docx,pdf,odt,rtf',
                            'size' => '10mb'
                        );

                        $filename = $file['name'];
                        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                        $upload = new Zend_File_Transfer_Adapter_Http();
                        $upload->addValidator('Extension', false, $v['ext'])
                               ->addValidator('Size', false, array('max' => $v['size']))
                               ->addValidator('Count', false, 1)
                               ->addFilter('Rename',$this->file_path.'/'.$rename)
                               ->setDestination($this->file_path);
                        
                        if(!$upload->isValid()){
                            return $this->postMessage('O arquivo deve possuir até '.$v['size'].'<br/> e ter uma das extensões a seguir: '.$v['ext'].'.','error');
                        }
                    }


                    $html = "<h1>Contato ".$assunto."</h1>". // monta html
                        // "<b>Assunto:</b> ".$assunto."<br/>".
                        // nl2br($r->getParam('mensagem'))."<br/><br/>".
                        "<b>Nome:</b> ".$r->getParam('nome')."<br/>".
                        "<b>CPF:</b> ".$r->getParam('cpf')."<br/>".
                        "<b>E-mail:</b> <a href='mailto:".
                        $r->getParam('email')."'>".$r->getParam('email').
                        "</a><br/>".
                        "<b>Telefone:</b> ".$r->getParam('telefone')."<br/>";
                    
                    $data_mensagem_contato = array(
                        'nome'     => $post['nome'],
                        // 'cpf'      => Is_Cpf::clean($post['cpf']),
                        'telefone' => Is_Cpf::clean($post['telefone']),
                        'email'    => $post['email'],
                        'assunto'  => $assunto,
                        // 'mensagem' => $post['mensagem'],
                        'data_cad' => date('Y-m-d H:i:s'),
                        'data_nasc'=> Is_Date::br2am($post['data_nasc']),
                        'mensagem_id' => $post['mensagem_id'],
                        'sexo'     => $post['sexo'],
                        'cidade'   => $post['cidade'],
                        'estado'   => $post['estado'],
                        'cursos'   => $post['cursos'],
                        'idiomas'  => $post['idiomas'],
                        'pretensao'=> $post['pretensao'],
                        'empresa'  => $post['empresa'],
                        'empresa2' => $post['empresa2'],
                        'empresa_data_inicio' => Is_Date::br2am($post['empresa_data_inicio']),
                        'empresa_data_saida'  => Is_Date::br2am($post['empresa_data_saida']),
                        'empresa_cargo'       => $post['empresa_cargo'],
                        'empresa2_data_inicio'=> Is_Date::br2am($post['empresa2_data_inicio']),
                        'empresa2_data_saida' => Is_Date::br2am($post['empresa2_data_saida']),
                        'empresa2_cargo'      => $post['empresa2_cargo'],
                        'area_interesse'      => $post['area_interesse'],
                        'escolaridade'        => $post['escolaridade'],
                    );

                    break;
            }

            try {
                if($file){ // upload final e cadastro de curriculo
                    $upload->receive();

                    $data_mensagem_contato['arquivo_id'] = $this->arquivos->insert(array(
                        "descricao" => $filename,
                        "path"     => $rename,
                        "data_cad" => date("Y-m-d H:i:s")
                    ));

                    $c_url = URL.'/public/files/'.$this->section.'/'.$rename;

                    $html.= '<br/><br/><a href="'.$c_url.'">Visualizar currículo</a><br/>';
                }

                // salva mensagem de contato
                $this->mensagens_contato->insert($data_mensagem_contato);

                // salvar em mailling (insertUpdate on duplicate)
                // $this->mailling->insertUpdate(array(
                //     'nome' => $post['nome'],
                //     'email' => $post['email'],
                //     'telefone' => Is_Cpf::clean($post['telefone']),
                //     'data_cad' => date('Y-m-d H:i:s'),
                // ));
                
                if(APPLICATION_ENV!='production1') { // tenta enviar emails de contato
                    Trupe_MaraRamos_Mail::sendWithReply( // email de contato
                        $post['email'],
                        $post['nome'],
                        'Contato '.$assunto,
                        $html,null,null,array(
                            'to' => ($post['mensagem_id']==11 && $email_assunto ? $email_assunto : $this->mensagem->email)
                        )
                    );

                    Trupe_MaraRamos_Mail::send( // resposta auto p/ cliente
                        $post['email'],
                        $post['nome'],
                        $this->mensagem->subject,
                        $this->mensagem->body
                    );
                }
                
                return $this->postMessage('Formulário enviado com sucesso!','msg',$redirect_url);
            } catch(Zend_Form_Exception $e){
                return $this->postMessage($form->getMessage(', ',true),'error',$redirect_url);
            } catch(Exception $e){
                return $this->postMessage('**'.$e->getMessage(),'error',$redirect_url);
            }
        }
        
        return $this->postMessage('* Preencha todos os campos <br>'.$form->getMessage(', ',true),'error',$redirect_url);
    }


}