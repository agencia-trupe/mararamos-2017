<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
    protected function _initRoute()
    {
        $this->router = new Zend_Controller_Router_Rewrite();
		$this->request =  new Zend_Controller_Request_Http();
		$this->router->route($this->request); // pegar todos os parametros
        
        $this->bootstrap('view');
        $this->view = $this->getResource('view');
        $this->bootstrap('layout');
        $this->layout = $this->getResource('layout');
        
        if($this->request->getModuleName()=="default"){
            // dados da empresa
            $dados_empresa = new Application_Model_Db_DadosEmpresa();
            $this->view->dados_empresa = Is_Array::utf8DbRow(
                $dados_empresa->fetchRow('id = 1')
            );
            $this->view->de = $this->view->dados_empresa;
        }
        
        if($this->request->getControllerName()=="admin"){
            $this->layout->setLayout("admin");
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session(SITE_NAME));
            
            $this->view->isLogged = $this->view->logged = $auth->hasIdentity();
            $this->view->login = $auth->hasIdentity() ? $auth->getIdentity() : null;
        }
		
        if(array_key_exists('busca',$this->request->getParams())){
            $this->view->busca = $this->request->getParam('busca');
        }
    }
    
    protected function _initLayoutConfigs(){
        $this->view->doctype('XHTML1_STRICT');
		
		switch($this->request->getControllerName()){
			case 'admin':
				$actionName = $this->request->getActionName();
				$actions = explode('/',$this->request->getRequestUri());
				$curAction = end($actions);
				$s = array_search($actionName,$actions);
				$action = $curAction!=$actions[$s]?$curAction:'index';
				//_d($action);
				$this->view->action = $action;
				$this->view->controller = $actionName;
				$this->view->module = $this->request->getControllerName();
				break;
			default:
				$this->view->action = $this->request->getActionName();
				$this->view->controller = $this->request->getControllerName();
				$this->view->module = $this->request->getModuleName();
		}
    }
    
    /**
	 * used for handling top-level navigation
	 * @return Zend_Navigation
	 */
	protected function _initNavigation()
	{
        if($this->request->getControllerName()=="admin"){   
            $login = new Zend_Session_Namespace(SITE_NAME.'_login');         
            $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation-admin.xml', 'nav');
            $config = $config->toArray();
            // _d(new Zend_Navigation($config['admin-top']));
            $this->view->menu = new stdClass();

            // lista paginas
            $pag = new Application_Model_Db_Paginas();
            $rows_pags = Is_Array::utf8DbResult($pag->fetchAll('status_id = 1',array('id')));
            
            if(count($rows_pags)){
                // $config['admin-top']['paginas']['pages'] = array();

                foreach($rows_pags as $row_pag){
                    $page = array(
                        'label' => $row_pag->titulo,
                        'uri'   => URL.'/admin/paginas/edit/'.$row_pag->id
                    );
                    
                    $config['admin-top']['paginas']['pages']['pagina-'.$row_pag->id] = $page;
                }
            }

            $config['admin-top']['paginas']['pages']['pagina-2']['class'] = 'has-sub-sub';
            $config['admin-top']['paginas']['pages']['pagina-2']['pages'] = array(
                'sobre' => array(
                    'label' => 'Sobre',
                    'uri'   => URL.'/admin/paginas/edit/2',
                ),
                'equipe' => array(
                    'label' => 'Equipe',
                    'uri'   => URL.'/admin/paginas/edit/6',
                ),
                'web' => array(
                    'label' => 'Na mídia - WEB',
                    'uri'   => URL.'/admin/webs',
                ),
                'impresso' => array(
                    'label' => 'Na mídia - Impresso',
                    'uri'   => URL.'/admin/impressos',
                ),
                'videos' => array(
                    'label' => 'Na mídia - Vídeos',
                    'uri'   => URL.'/admin/videos',
                ),
            );

            // lista mensagens contato
            // $msg = new Application_Model_Db_Mensagens();
            // $msgs = $msg->fetchAll('status_id=1 and id >= 11','titulo');

            // if ($msgs) {
            //     $menu_msgs = array();
            //     $menu_msgs['mensagens-todas'] = array(
            //         'label' => 'Todas',
            //         'uri'   => URL.'/admin/mensagens-contato',
            //     );

            //     foreach ($msgs as $m) {
            //         $menu_msgs['mensagens-'.$m->id] = array(
            //             'label' => $m->titulo,
            //             'uri'   => URL.'/admin/mensagens-contato/?search-by=mensagem_id&search-txt='.$m->id,
            //             // 'uri'   => URL.'/admin/mensagens-contato/index/search-by/mensagem_id/search-txt/'.$m->id,
            //         );
            //     }

            //     // _d($menu_msgs);
            //     $config['admin-top']['mensagens']['pages']['recebidas']['class'] = 'has-sub-sub';
            //     $config['admin-top']['mensagens']['pages']['recebidas']['pages'] = $menu_msgs;
            // }

            self::configUrlPrefix($config['admin-top']);
            $this->view->menu->admin_top  = new Zend_Navigation($config['admin-top']);
        } else {
            $_menu_nav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
            $menu_nav = $_menu_nav->toArray();
            $menu_nav_top = $menu_nav['top'];

            // prefixos
            self::configUrlPrefix($menu_nav_top);
            self::configUrlPrefix($menu_nav['footer']);
			
            $this->view->menu = new stdClass();
            $this->view->menu->top        = new Zend_Navigation($menu_nav_top);
            $this->view->menu->footer    = new Zend_Navigation($menu_nav['footer']);
			
            $uri = APPLICATION_ENV == 'development' ?
                    URL.$this->request->getPathInfo() : // dev
                    URL.$this->request->getPathInfo();  // production
            
            foreach(get_object_vars($this->view->menu) as $menu){
                $activeNav = $menu->findByUri($uri) or
                $activeNav = $menu->findByUri(str_replace('http://'.$_SERVER['HTTP_HOST'],'',$uri));
                
                if(null !== $activeNav){
                    $activeNav->active = true;
                    $activeNav->setClass($activeNav->getClass()." active");	
                }
            }
        }
	}

    function configUrlPrefix(&$config)
    {
        foreach($config as &$c){
            // adiciona url ao link
            if(isset($c['uri'])) if($c['uri'] != '#' & !strstr($c['uri'],'http')) $c['uri'] = URL.$c['uri'];
            // adiciona recursão à função
            if(isset($c['pages'])) $c['pages'] = self::configUrlPrefix($c['pages']);
        }
        
        return $config;
    }
}