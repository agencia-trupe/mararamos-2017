<?php

class Application_Model_Db_Webs extends ZendPlugin_Db_Table 
{
    protected $_name = "webs";
    protected $_foto_join_table = 'webs_fotos'; // tabela de relação para registros de fotos
    protected $_foto_join_table_field = 'web_id'; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    
    /**
     * Retorna registro por alias
     */
    public function getByAlias($alias)
    {
        return $this->fetchRow('alias = "'.$alias.'"');
    }
    
    /**
     * Retorna as fotos da promoção
     *
     * @param int  $id    - id da promoção
     * @param bool $check - checar se promoção existe?
     *
     * @return array - rowset com fotos da promoção
     */
    public function getFotos1($id,$check=true)
    {
        $web = $this->fetchRow('id="'.$id.'"');

        if($check && !$web) return false;
        
        $fotos = array();
        
        if($web_fotos = $web->findDependentRowset('Application_Model_Db_WebsFotos')){
            foreach($web_fotos as $web_foto){
                $fotos[] = Is_Array::utf8DbRow($web_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
            }
        }
        
        return $fotos;
    }

    /**
     * Retorna fotos dos webs
     * 
     * @param array $webs - rowset de webs para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - webs com fotos
     */
    public function getFotos($webs,$withObjects=false)
    {
        $pids = array(); // ids de webs

        // identificando webs
        foreach($webs as $produto) $pids[] = $produto->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('webs_fotos as fc',array('fc.web_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.web_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            $fotos = $select->query()->fetchAll();
            $fotos = array_map('Is_Array::toObject',$fotos);
        } else {
            $_webs_fotos = new Application_Model_Db_WebsFotos();
            $fotos = $_webs_fotos->fetchAll('web_id in ('.implode(',',$pids).')');
        }

        // associando fotos
        foreach($webs as &$produto){
            $produto->fotos = $this->getFotosSearch($produto->id,$fotos);
        }

        return $webs;
    }

    /**
     * Monta rowset de fotos com base no produto_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->web_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}