<?php

class Application_Model_Db_Fotos extends ZendPlugin_Db_Table {
    protected $_name = "fotos";
    
    protected $_dependentTables = array(
        'Application_Model_Db_ProdutosFotos',
        'Application_Model_Db_PromocoesFotos',
        'Application_Model_Db_DestaquesFotos',
        'Application_Model_Db_Categorias',
    );
    
    protected $_referenceMap = array(
        'Application_Model_Db_ProdutosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutosFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_PromocoesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_PromocoesFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_DestaquesFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_DestaquesFotos',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_Categorias' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Categorias',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_Cuidados' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Cuidados',
            'refColumns'    => 'foto_id'
        ),
        'Application_Model_Db_BlogsFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_BlogsFotos',
            'refColumns'    => 'foto_id'
        )
    );
    
    /**
     * Retorna fotos de uma tabela de referencia
     * 
     * @param string $table - nome da tabela que possui as fotos (ex.: noticias)
     * @param int    $id    - id do row a serem selecionadas as fotos (ex.: noticia->id: 1)
     * @param string $field - campo de referencia da tabela com fotos (ex.: noticia_id)
     *                        :se não for passado (null), assume que o campo "foto_id" 
     *                         está na mesma tabela (noticias)
     * @param array $params - parametros adicionais para model Db_Table:
     *                        where, order, limit, offset
     * 
     * @return array - rowset com as fotos
     */
    public function getFrom($table,$id,$field=null,$params=null)
    {
        return $this->q(
            'select '.
                'f.* '.
                ',t1.'.($field ? $field : 'id ').' as row_id '.
            'from '.$table.($field ? '_fotos' : '').' as t1 '.
                'left join fotos f on f.id = t1.foto_id '.
            'where '.
                ($field ?
                    't1.'.$field.' = "'.$id.'" ' :
                    't1.id = "'.$id.'" ').
                (isset($params['where']) ? 'and ('.$params['where'].') ' : '').
            (isset($params['group']) ? 'group by '.$params['group'].' ' : '').
            (isset($params['order']) ? 'order by '.$params['order'].' ' : '').
            (isset($params['offset']) || isset($params['limit']) ? 'limit '.(isset($params['offset'])?$params['offset'].',':'').$params['limit'].' ' : '')
        );
    }
}
