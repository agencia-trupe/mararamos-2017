<?php

class Application_Model_Db_WebsFotos extends Zend_Db_Table
{
    protected $_name = "webs_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Webs');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Webs' => array(
            'columns' => 'web_id',
            'refTableClass' => 'Application_Model_Db_Webs',
            'refColumns'    => 'id'
        )
    );
}
