<?php
/**
 * Modelo da tabela de usuarios
 */
class Application_Model_Db_Paginas extends ZendPlugin_Db_Table 
{
    protected $_name = "paginas";
    
    protected $_dependentTables = array('Aluno_Model_Pessoa');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Pessoa' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Pessoa',
            'refColumns'    => 'usuario_id'
        )
    );

    public function getPaginas($ids,$withFotos=true,$parent_id=null)
    {
        if(is_array($ids)) $ids = implode(',', $ids);
        if(!(bool)trim($ids)) $ids = '0';

        return $this->getPaginasWhere(
            'status_id = 1 and (id in ('.$ids.')) '.
                (($parent_id) ? 'and parent_id = "'.$parent_id.'" ' : ''),
            $withFotos,
            $parent_id
        );
    }

    public function getPaginasWhere($where='status_id=1',$withFotos=true,$parent_id=null)
    {
        $_paginas = Is_Array::utf8DbRows($this->fetchAll($where,'ordem'));
        if(!$_paginas) return null;

        if($withFotos){
            $ids = array(); // pegando ids de paginas
            foreach ($_paginas as $pag) $ids[] = $pag->id;
            $ids = (count($ids)) ? implode(',', $ids) : '0';
            $_pfs = $this->getFotos($ids); // fotos
            $_pffs = $this->getFotosFixas($ids,true); // fotos_fixas multi

            $pf = array(); // paginas_fotos
            foreach($_pfs as $_pf) {
                if(!isset($pf[$_pf->pagina_id])) $pf[$_pf->pagina_id] = array();
                $pf[$_pf->pagina_id][] = $_pf;
            }

            $pff = array(); // paginas_fotos_fixas
            foreach($_pffs as $_pff) {
                if(!isset($pff[$_pff->pagina_id])) $pff[$_pff->pagina_id] = array();
                $pff[$_pff->pagina_id][] = $_pff;
            }
        }
        
        $paginas = array();
        foreach($_paginas as $_pag) {
            $pag = $_pag;
            $pag->fotos = @$pf[$pag->id];
            $pag->fotos_fixas = @$pff[$pag->id];
            if((bool)$pag->fotos_fixas) $pag->foto_fixa = $pag->fotos_fixas[0];

            $paginas[$_pag->id] = $pag;
        }

        return $paginas;
    }

    public function getPagina($alias,$withFotos=true,$parent_id=null,$active=true)
    {
        $pagina = Is_Array::utf8DbRow($this->fetchRow(
            '(alias = "'.$alias.'" or id = "'.$alias.'") '.($active ? ' and status_id = 1 ' : '').
            (($parent_id) ? 'and parent_id = "'.$parent_id.'" ' : '')
        ));
        if(!$pagina) return null;

        if($withFotos){
            // $pagina->fotos = ($pagina->allow_photos) ? $this->getFotos($pagina->id) : null;
            $pagina->fotos = $this->getFotos($pagina->id);
            $pagina->fotos_fixas = $this->getFotosFixas($pagina->id);
        }

        return $pagina;
    }

    public function getFotosFixas($id=null,$multi=false)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos_fixas as tf')
            ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $multi ? $fotos : $fotos[0];
    }

    public function getFotos($id=null)
    {
    	$select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('paginas_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('pagina_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }
}