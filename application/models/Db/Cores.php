<?php

class Application_Model_Db_Cores extends ZendPlugin_Db_Table
{
    protected $_name = "cores";
    protected $default_order = 'ordem'; // ordem padrão para ordenação dos registros
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_ProdutosFotos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_ProdutosFotos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_ProdutosFotos',
            'refColumns'    => 'cor_id'
        )
    );
}