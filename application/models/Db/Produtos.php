<?php

class Application_Model_Db_Produtos extends ZendPlugin_Db_Table 
{
	protected $_name = "produtos";
	protected $_foto_join_table = 'produtos_fotos';
	protected $_foto_join_table_field = 'produto_id';

	public static $tipos = array(
		1 => 'Projetos',
		2 => 'Inspiração',
		3 => 'Mobiliário Exclusivo'
	);

	public static $generos = array(
		0 => 'Homens',
		1 => 'Mulheres',
		2 => 'Unissex',
		3 => 'Crianças'
	);

	public static $roles = array(
		'0' => 'Todos',
		'1' => 'Usuários cadastrados',
		'2' => 'Clientes VIP'
	);

	public static $ordem_combo = array(
		'novos' => 'Mais novos',
		'valor-maior' => 'Maior valor',
		'valor-menor' => 'Menor valor',
		'titulo' => 'Título',
	);

	public static $ordem_table = array(
		'novos' => 'data_cad desc',
		'titulo' => 'titulo',
		'valor-maior' => 'valor desc',
		'valor-menor' => 'valor asc',
	);
	
	/**
	 * Referências
	 */
	protected $_dependentTables = array('Application_Model_Db_Categorias','Application_Model_Db_ProdutosSugestoes','Application_Model_Db_SugestoesItems');
	
	protected $_referenceMap = array(
		'Application_Model_Db_Categorias' => array(
			'columns' => 'categoria_id',
			'refTableClass' => 'Application_Model_Db_Categorias',
			'refColumns'    => 'id'
		),
		'Application_Model_Db_ProdutosSugestoes' => array(
			'columns' => 'id',
			'refTableClass' => 'Application_Model_Db_ProdutosSugestoes',
			'refColumns'    => 'sugestao_id'
		),
		'Application_Model_Db_SugestoesItems' => array(
			'columns' => 'id',
			'refTableClass' => 'Application_Model_Db_SugestoesItems',
			'refColumns'    => 'produto_id'
		),
	);

	public function getGeneros()
	{
		return self::$generos;
	}

	public function getGenerosAssoc($tolower=false)
	{
		$generos = array();

		foreach(self::$generos as $k=>$v) $generos[($tolower)?strtolower($v):$v] = $k;

		return $generos;
	}

	public function getGenero($key=0)
	{
		return self::$generos[$key];
	}
	
	/**
	 * Retorna produto com suas imagens com base no alias se @alias for string ou id se @alias for numérico
	 *
	 * @param string|int $alias - valor do alias ou id do produto
	 *
	 * @return object|bool - objeto contendo o produto com suas imagens e categoria ou false se não for encontrado
	 */
	public function getWithFotos($alias)
	{
		$column = is_numeric($alias) ? 'id' : 'alias';
		if(!$produto = $this->fetchRow($column.'="'.$alias.'"')){
			return false;
		}
		$fotos = array();
		
		// if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos')){
		//     foreach($produto_fotos as $produto_foto){
		//         $pf = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
				
		//         $pf->cor_id = ($produto_foto->cor_id) ? $produto_foto->cor_id : null;
		//         $pf->cor = ($produto_foto->cor_id) ?
		//                     Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Cores')->current()) :
		//                     null;
				
		//         $fotos[] = $pf;
		//     }
		// }
		// _d($fotos);
		
		$object = Is_Array::utf8DbRow($produto);
		$object->categoria = (bool)$produto->categoria_id ?
							Is_Array::utf8DbRow($produto->findDependentRowset('Application_Model_Db_Categorias')->current()) :
							null;
		
		$fotos = $this->getFotos(array($object),1);
		// $object->fotos = $fotos;
		// _d($object);
		
		// categorias
		$this->getCategorias(array($object),1);
		if(!$object->categoria && $object->categorias)
			$object->categoria = reset($object->categorias);

		// descontos
		$object = self::checkDesconto($object);

		// foto logotipo
		$object->foto = ($object->foto_id)
			? $this->s1('fotos','*','id = "'.$object->foto_id.'"')
			: null;
		
		return $object;
	}
	
	public function getDestaques($limit=3)
	{
		if($produtos = $this->fetchAll('destaque = 1','data_edit desc',$limit)){
			$destaques = array();
			
			foreach($produtos as $produto){
				$p = Is_Array::utf8DbRow($produto);
				$p->fotos = $this->getFotos($p->id);
				$destaques[] = $p;
			}
			
			return $destaques;
		}
		
		return false;
	}
	
	/**
	 * Retorna as sugestoes do produto
	 *
	 * @param int  $id        - id do produto
	 * @param bool $get_fotos - selecionar também fotos?
	 * @param int  $limit     - limite da lista
	 * @param bool $rand      - ordenação randômica?
	 *
	 * @return array - rowset com sugestoes do produto
	 */
	public function getSugestoes($id,$get_fotos=false,$limit=null,$rand=false)
	{
		// saímos se o produto não existir
		if(!$produto = $this->fetchRow('id="'.$id.'"')){
			return false;
		}
		$sugestoes = array();
		
		// montando a array de sugestões
		$select = $rand ? // randomizamos os resultados se for solicitado
				  $this->select()->order(new Zend_Db_Expr('RAND()'))->limit($limit) :
				  null;
		if($produto_sugestoes = $produto->findDependentRowset('Application_Model_Db_ProdutosSugestoes',null,$select)){
			foreach($produto_sugestoes as $produto_sugestao){
				$sugestao = Is_Array::utf8DbRow($produto_sugestao->findDependentRowset('Application_Model_Db_Produtos')->current());
				$sugestao = $this->checkDesconto($sugestao);
				$sugestoes[] = $sugestao;
			}
		}
		
		// pegamos as fotos se for solicitado
		if($get_fotos){
			$produtos_fotos = new Application_Model_Db_ProdutosFotos();
			
			foreach($sugestoes as &$sugestao){
				$sugestao->fotos = array();
				
				if($produto_fotos = $produtos_fotos->fetchAll('produto_id='.$sugestao->id)){    
					foreach($produto_fotos as $produto_foto){
						$sugestao->fotos[] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current();
					}
				}
			}
		}
		
		// randomizamos os resultados
		// - retirado para melhor performance caso haja muitas sugestões, solução adicionada acima
		//if($rand){ shuffle($sugestoes); }
		return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;
	}
	
	/**
	 * Retorna quantidade total
	 * 
	 * @param string $where - string de seleção where, padrão NULL
	 *
	 * @return int
	 */
	public function count($where=null)
	{
		$select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
		$select->from($this->_name,array('count(*) as cnt'));
		$count = $where ? $select->where($where)->query()->fetchAll() : $select->query()->fetchAll();
		return $count[0]['cnt'];
	}
	
	/**
	 * Checa descontos do produto (recursivo)
	 *
	 * $param array|object $object - Array/Objeto do produto para ser checado os descontos.
	 *                               Não pode ser uma instância do Zend Table.
	 *
	 * @return array|object
	 */
	public function checkDesconto($object=null)
	{
		if($object===null) return null;
		
		if(is_array($object)){ // adiciona resursão à função
			if(count($object)){
				foreach($object as &$o) $o = self::checkDesconto($o);
				return $object;
			}
			return null;
		}
		
		if((bool)(float)$object->valor_promo){ // se possui valor promocional
			$object->valor_desconto = $object->valor; // adiciona valor_desconto como o valor original
			$object->valor = $object->valor_promo; // seta valor com desconto, para ser usado como o valor do produto
		} else {
			if((bool)$object->categoria_id){
			  $cats = new Application_Model_Db_Categorias();
			  $cat  = $cats->fetchRow('id='.$object->categoria_id);
			  
			  if((bool)$cat->desconto){ // se a categoria está com desconto
				  $object->valor_desconto = $object->valor;
				  $object->valor = Is_Math::percentDec($cat->desconto,$object->valor);
			  } else {
				  $object->valor_desconto = null;
			  }
			}
		}
		
		return $object;
	}

	/**
	 * Retorna as formatos do produto
	 *
	 * @param int  $id        - id do produto
	 * @param int  $limit     - limite da lista
	 * @param bool $rand      - ordenação randômica?
	 *
	 * @return array - rowset com sugestoes do produto
	 */
	public function getFormatos($id,$limit=null,$rand=false)
	{
		$_sugestoes = new Application_Model_Db_ProdutoFormato();
		$_tags = new Application_Model_Db_Formatos();
		$sugestoes = array(); $sugestoes_ids = array();

		if(!(bool)$prods = $_sugestoes->fetchAll('produto_id="'.$id.'"')){
			return false;
		}

		foreach($prods as $ps) $sugestoes_ids[] = $ps->formato_id;

		$sugestoes = Is_Array::utf8DbResult($_tags->fetchAll('id in ('.implode(',',$sugestoes_ids).')','formato'));
		
		return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;

		/* old
		foreach($prods as $ps){
			$s = Is_Array::utf8DbRow($_tags->fetchRow('id='.$ps->formato_id));
			$sugestoes[] = $s;
		}
		
		return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;*/
	}

	/**
	 * Retorna as gramaturas do produto
	 *
	 * @param int  $id        - id do produto
	 * @param int  $limit     - limite da lista
	 * @param bool $rand      - ordenação randômica?
	 *
	 * @return array - rowset com sugestoes do produto
	 */
	public function getGramaturas($id,$limit=null,$rand=false)
	{
		$_sugestoes = new Application_Model_Db_ProdutoGramatura();
		$_tags = new Application_Model_Db_Gramaturas();
		$sugestoes = array();

		if(!(bool)$prods = $_sugestoes->fetchAll('produto_id="'.$id.'"')){
			return false;
		}

		foreach($prods as $ps) $sugestoes_ids[] = $ps->gramatura_id;

		$sugestoes = Is_Array::utf8DbResult($_tags->fetchAll('id in ('.implode(',',$sugestoes_ids).')','gramatura'));
		
		return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;

		/* old
		foreach($prods as $ps){
			$s = Is_Array::utf8DbRow($_tags->fetchRow('id='.$ps->gramatura_id));
			$sugestoes[] = $s;
		}
		
		return $limit ? array_slice($sugestoes,0,$limit) : $sugestoes;*/
	}

	/**
	 * Retorna categorias dos produtos
	 * 
	 * @param array $produtos - rowset de produtos para associar categorias
	 * @param bool  $withObjects - se irá retornar as categorias ou somente id
	 * 
	 * @return array of objects - produtos com categorias
	 */
	public function getCategorias($produtos,$withObjects=false)
	{
		$pids = array(); // ids de produtos

		// identificando produtos
		foreach($produtos as $produto) $pids[] = $produto->id;

		if($withObjects){ // se o retorno for com objetos
			$select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
			$select->from('produtos_categorias as pc',array('pc.produto_id'))
				   ->joinLeft('categorias as c','c.id = pc.categoria_id',array('*'))
				   ->where('pc.produto_id in ('.implode(',',$pids).')');
			
			$categorias = $select->query()->fetchAll();
			$categorias = array_map('Is_Array::utf8All',$categorias);
			$categorias = array_map('Is_Array::toObject',$categorias);

			// pegando fotos
			foreach ($categorias as $cat) {
				if($cat->foto_id || $cat->foto2_id) {
					$f = ($cat->foto_id) ?
						$this->s1('fotos','path,titulo','id="'.$cat->foto_id.'"') :
						null;
					$f2= ($cat->foto2_id) ?
						$this->s1('fotos','path,titulo','id="'.$cat->foto2_id.'"') :
						null;

					if($f) {
						$alias = '';
						if((bool)trim($f->titulo)) $alias = Is_Str::toUrl($f->titulo).'-';
						else if($cat->alias) $alias = $cat->alias.'-';
						$f->path = $alias.$f->path;
					}
					if($f2) {
						$alias = '';
						if((bool)trim($f2->titulo)) $alias = Is_Str::toUrl($f2->titulo).'-';
						else if($cat->alias) $alias = $cat->alias.'-';
						$f2->path = $alias.$f2->path;
					}

					$cat->foto_path = $f->path;
					$cat->foto2_path = $f2->path;
				}
			}
		} else {
			$_produtos_categorias = new Application_Model_Db_ProdutosCategorias();
			$categorias = $_produtos_categorias->fetchAll('produto_id in ('.implode(',',$pids).')');
		}

		// associando categorias
		foreach($produtos as &$produto){
			$produto->categorias = $this->getCategoriasSearch($produto->id,$categorias);
		}

		return $produtos;
	}

	/**
	 * Monta rowset de categorias com base no produto_id ($pid)
	 */
	public function getCategoriasSearch($pid=null,$cats=array())
	{
		$categorias = array();

		foreach($cats as $cat) if($cat->produto_id == $pid) $categorias[] = $cat;
		
		return $categorias;
	}

	/**
	 * Retorna as fotos do produto
	 *
	 * @param int $id - id do produto
	 *
	 * @return array - rowset com fotos do produto
	 */
	public function getFotosById($id,$cor=null)
	{
		if(!$produto = $this->fetchRow('id="'.$id.'"')) return null;
		$fotos = array();
		
		if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos')){
			foreach($produto_fotos as $produto_foto){
				$f = Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Fotos')->current());
				$f->cor_id = $produto_foto->cor_id ? $produto_foto->cor_id : null;
				$f->cor    = $produto_foto->cor_id ? Is_Array::utf8DbRow($produto_foto->findDependentRowset('Application_Model_Db_Cores')->current()) : null;
				
				if($cor!==null) {
					if ($cor==$f->cor_id) $fotos[] = $f;
				} else {
					$fotos[] = $f;
				}
			}
		}
		
		return $fotos;
	}

	/**
	 * Retorna fotos dos produtos
	 * 
	 * @param array $produtos - rowset de produtos para associar fotos
	 * @param bool  $withObjects - se irá retornar as fotos ou somente id
	 * 
	 * @return array of objects - produtos com fotos
	 */
	public function getFotos($produtos,$withObjects=false)
	{
		if(!$produtos) return $produtos;
		$pids = array(); // ids de produtos

		// identificando produtos
		foreach($produtos as $produto) $pids[] = $produto->id;

		if($withObjects){ // se o retorno for com objetos
			$select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
			$select->from('produtos_fotos as fc',array('fc.produto_id'))
				   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
				   ->where('fc.produto_id in ('.(count($pids) ? implode(',',$pids) : '0').')')
				   ->order('f.ordem');
			
			$fotos = $select->query()->fetchAll();
			$fotos = array_map('Is_Array::toObject',$fotos);
		} else {
			$_produtos_fotos = new Application_Model_Db_ProdutosFotos();
			$fotos = $_produtos_fotos->fetchAll('produto_id in ('.implode(',',$pids).')');
		}

		// associando fotos
		foreach($produtos as &$produto){
			$produto->fotos = $this->getFotosSearch($produto->id,$fotos);

			// if($produto->fotos) foreach($produto->fotos as $f) {
			//     $alias = '';
			//     if((bool)trim($f->titulo)) $alias = Is_Str::toUrl($f->titulo).'-';
			//     else if(isset($produto->alias)) $alias = $produto->alias.'-';
			//     $f->path = $alias.$f->path;
			// }

			$produto->foto_path = $produto->fotos ? $produto->fotos[0]->path : null;
		}

		return $produtos;
	}

	/**
	 * Monta rowset de fotos com base no produto_id ($pid)
	 */
	public function getFotosSearch($pid=null,$cats=array())
	{
		$fotos = array();

		foreach($cats as $cat) if($cat->produto_id == $pid) $fotos[] = $cat;
		
		return $fotos;
	}

	public function getRoles($id=null)
	{
		return $id ? self::$roles[$id] : self::$roles;
	}

	public function getOrdemCombo($id=null,$first=false,$vip=false)
	{
		if(is_array($first)) $first = $first;
		else if($first) $first = array(''=>'Ordenar por');
		else $first = array();

		if(!$vip && !$id){
		  $ordem = $first + self::$ordem_combo;
		  unset($ordem['valor-maior']);
		  unset($ordem['valor-menor']);
		  return $ordem;
		}

		return $id ? self::$ordem_combo[$id] : $first + self::$ordem_combo;
	}

	public function getOrdemTable($id=null)
	{
		return $id ? self::$ordem_table[$id] : self::$ordem_table;
	}

	public static function tipos($id=null)
	{
		return $id ? @self::$tipos[$id] : self::$tipos;
	}
	public function getTipos($id=null){ return self::tipos($id); }
	
	public static function tipoByAlias($alias)
	{
		$tipo = null;
		foreach (self::tipos() as $k => $t)
			if(Is_Str::toUrl($t)==$alias) $tipo = $k;
		return $tipo;
	}
	public function getTipoByAlias($alias){ return self::tipoByAlias($alias); }

}