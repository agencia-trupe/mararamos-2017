<?php

class Application_Model_Db_ImpressosFotos extends Zend_Db_Table
{
    protected $_name = "impressos_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Impressos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Impressos' => array(
            'columns' => 'impresso_id',
            'refTableClass' => 'Application_Model_Db_Impressos',
            'refColumns'    => 'id'
        )
    );
}
