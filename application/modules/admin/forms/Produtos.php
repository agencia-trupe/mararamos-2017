<?php

class Admin_Form_Produtos extends ZendPlugin_Form
{
    protected $tipo = 1;

    public function __construct($options=null,$tipo=null)
    {
        if($tipo) $this->tipo = $tipo;
        return parent::__construct($options);
    }

    public function init()
    {
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/produtos/save')
             ->setAttrib('id','frm-produtos')
             ->setAttrib('name','frm-produtos');
        
        // $categs = new Application_Model_Db_Categorias();
        // $generos = Application_Model_Db_Produtos::getGeneros();
        // $roles = Application_Model_Db_Produtos::getRoles();
        
        // elementos
        // $this->addElement('select','tipo',array('label'=>'Tipo','class'=>'txt','multiOptions'=>Application_Model_Db_Produtos::tipos()));
        $this->addElement('hidden','tipo');
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues('descricao',1)));
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getParentsKV($categs->getWithChildren(),array('__none__'=>'Selecione...'),true)));
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        
        if(!in_array($this->tipo, array(3))){
            $this->addElement('text','tipo_projeto',array('label'=>'Tipo de projeto','class'=>'txt'));
            $this->addElement('text','local',array('label'=>'Local','class'=>'txt'));
            $this->addElement('text','area',array('label'=>'Área do projeto','class'=>'txt'));
            $this->addElement('text','desc_equipe',array('label'=>'Equipe','class'=>'txt'));
            $this->addElement('text','desc_arquitetura',array('label'=>'Arquitetura','class'=>'txt'));
            $this->addElement('text','desc_interiores',array('label'=>'Interiores','class'=>'txt'));
            $this->addElement('text','desc_fotos',array('label'=>'Fotos','class'=>'txt'));
        }
        
        // $this->addElement('text','cod',array('label'=>'Código','class'=>'txt'));
        //$this->addElement('text','peso',array('label'=>'Peso','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','valor',array('label'=>'Valor R$','class'=>'txt txt3 mask-currency'));
        // //$this->addElement('text','valor_promo',array('label'=>'Valor Promo. R$','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','obs_valor',array('label'=>'Obs. Valor','class'=>'txt txt2'));
        // $this->addElement('text','medida_c',array('label'=>'Comp.','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','medida_l',array('label'=>'Larg.','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','medida_a',array('label'=>'Alt.','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','estoque',array('label'=>'Estoque','class'=>'txt txt3 mask-int'));
        // $this->addElement('text','estoque_minimo',array('label'=>'Estoque mín.','class'=>'txt txt3 mask-int'));
        // $this->addElement('radio','genero',array('label'=>'Gênero','class'=>'radio','multiOptions'=>$generos,'separator'=>' '));
        //$this->addElement('select','genero',array('label'=>'Gênero','class'=>'txt','multiOptions'=>array(0=>'Feminino',1=>'Masculino')));
        $this->addElement('textarea','descricao',array('label'=>'Descrição','class'=>'txt'));
        // $this->addElement('textarea','descricao_completa',array('label'=>'Detalhes','class'=>'txt wysiwyg'));
        // $this->addElement('select','role',array('label'=>'Visível para','class'=>'txt','multiOptions'=>$roles));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        // $this->addElement('checkbox','novo',array('label'=>'Novidade'));
        
        // atributos
        $this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        // $this->getElement('descricao_completa')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('categoria_id')->setRequired();
        $this->getElement('titulo')->setRequired();
        // $this->getElement('cod')->setRequired();
        // $this->getElement('genero')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

