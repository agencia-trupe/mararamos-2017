<?php

class Admin_Form_Webs extends ZendPlugin_Form
{
    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/webs/save')
             ->setAttrib('id','frm-webs')
             ->setAttrib('name','frm-webs');
        
        // elementos
        $this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        $this->addElement('hidden','alias');
        //$this->addElement('checkbox','allow_files',array('label'=>'Arquivos'));
        //$this->addElement('checkbox','allow_photos',array('label'=>'Inserir imagens?'));
        
        //$this->addElement('text','link',array('label'=>'Link <input type="radio" class="radios link" id="check_link_0" name="check_link" value="0" />','class'=>'txt'));
        //$this->addElement('textarea','body',array('label'=>'Texto <input type="radio" class="radios body" id="check_link_1" name="check_link" value="1" />','class'=>'txt wysiwyg'));
        $this->addElement('text','link',array('label'=>'Link','class'=>'txt'));
        // $this->addElement('textarea','body',array('label'=>'Texto','class'=>'txt'));
        
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        // $this->addElement('checkbox','show_title',array('label'=>'Mostrar título'));
        // $this->addElement('checkbox','capa',array('label'=>'Capa de promoções'));
        
        // atributos
        // $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('titulo')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

