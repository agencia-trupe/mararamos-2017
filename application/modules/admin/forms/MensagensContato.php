<?php

class Admin_Form_MensagensContato extends ZendPlugin_Form
{

    public function init()
    {
        // configurações do form
        $this->setMethod('post')->setAction(URL.'/admin/mensagens-contato/save')
             ->setAttrib('id','frm-mensagens-contato')
             ->setAttrib('name','frm-mensagens-contato');
        
        $mensagens = null;
        if($this->mensagens) $mensagens = $this->mensagens;
        else {
            $msgs = new Application_Model_Db_Mensagens();
            $mensagens = $msgs->getKeyValues('titulo',false,'subject','status_id=1 and id>=11');

        }

        // elementos
        $this->addElement('select','mensagem_id',array('label'=>'Treinamento:','class'=>'txt','multiOptions'=>$mensagens));
        $this->addElement('hidden','titulo',array('class'=>'txt'));
        $this->addElement('text','subject',array('label'=>'Assunto','class'=>'txt'));
        $this->addElement('text','email',array('label'=>'E-mail de destino','class'=>'txt'));
        $this->addElement('textarea','body',array('label'=>'Conteúdo','class'=>'txt wysiwyg'));
        
        // atributos
        $this->getElement('body')->setAttrib('rows',15)->setAttrib('cols',1);
        
        // filtros / validações
        $this->getElement('subject')->setRequired();
        $this->getElement('email')->setRequired()->addValidator('EmailAddress')
             ->addFilter('StripTags')->addFilter('StringTrim')->addValidator('NotEmpty')
             ->setAttrib('data-validate',true)->setAttrib('data-errmsg','E-mail inválido');
        
        // remove decoradores
        $this->removeDecs();
    }
}

