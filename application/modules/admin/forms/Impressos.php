<?php

class Admin_Form_Impressos extends ZendPlugin_Form
{
    public function init()
    {
        // configurações
        $this->setMethod('post')->setAction(URL.'/admin/impressos/save')
             ->setAttrib('id','frm-impressos')
             ->setAttrib('name','frm-impressos');
		
        // $categs = new Application_Model_Db_Categorias();
        // $generos = Application_Model_Db_Impressos::getGeneros();
        // $roles = Application_Model_Db_Impressos::getRoles();
        
        // elementos
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getKeyValues('descricao',1)));
        // $this->addElement('select','categoria_id',array('label'=>'Categoria','class'=>'txt','multiOptions'=>$categs->getParentsKV($categs->getWithChildren(),array('__none__'=>'Selecione...'),true)));
		$this->addElement('text','titulo',array('label'=>'Título','class'=>'txt'));
        // $this->addElement('text','cod',array('label'=>'Código','class'=>'txt'));
        //$this->addElement('text','peso',array('label'=>'Peso','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','valor',array('label'=>'Valor R$','class'=>'txt txt3 mask-currency'));
        // //$this->addElement('text','valor_promo',array('label'=>'Valor Promo. R$','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','obs_valor',array('label'=>'Obs. Valor','class'=>'txt txt2'));
        // $this->addElement('text','medida_c',array('label'=>'Comp.','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','medida_l',array('label'=>'Larg.','class'=>'txt txt3 mask-currency'));
        // $this->addElement('text','medida_a',array('label'=>'Alt.','class'=>'txt txt3 mask-currency'));
		// $this->addElement('text','estoque',array('label'=>'Estoque','class'=>'txt txt3 mask-int'));
		// $this->addElement('text','estoque_minimo',array('label'=>'Estoque mín.','class'=>'txt txt3 mask-int'));
        // $this->addElement('radio','genero',array('label'=>'Gênero','class'=>'radio','multiOptions'=>$generos,'separator'=>' '));
        //$this->addElement('select','genero',array('label'=>'Gênero','class'=>'txt','multiOptions'=>array(0=>'Feminino',1=>'Masculino')));
        // $this->addElement('textarea','descricao',array('label'=>'Descrição','class'=>'txt wysiwyg','maxlength'=>'255'));
        // $this->addElement('textarea','descricao_completa',array('label'=>'Detalhes','class'=>'txt wysiwyg'));
        // $this->addElement('select','role',array('label'=>'Visível para','class'=>'txt','multiOptions'=>$roles));
        $this->addElement('checkbox','status_id',array('label'=>'Ativo'));
        // $this->addElement('checkbox','destaque',array('label'=>'Destaque'));
        // $this->addElement('checkbox','novo',array('label'=>'Novidade'));
        
        // atributos
        // $this->getElement('descricao')->setAttrib('rows',1)->setAttrib('cols',1);
        // $this->getElement('descricao_completa')->setAttrib('rows',1)->setAttrib('cols',1);
        
        // filtros / validações
        // $this->getElement('categoria_id')->setRequired();
        $this->getElement('titulo')->setRequired();
        // $this->getElement('cod')->setRequired();
        // $this->getElement('genero')->setRequired();
        
        // remove decoradores
        $this->removeDecs();
    }
}

