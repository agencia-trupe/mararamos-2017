<?php

class Admin_ImpressosController extends ZendPlugin_Controller_Ajax
{

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "NA MÍDIA IMPRESSOS";
        $this->view->section = $this->section = "impressos";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
        
        $this->view->MAX_FOTOS = 12;
        
        // models
        $this->impressos = new Application_Model_Db_Impressos();
        // $this->categorias = new Application_Model_Db_Categorias();
        // $this->impressos_infos = new Application_Model_Db_ImpressosInfos();
        // $this->impressos_aviseme = new Application_Model_Db_ImpressosAviseme();
        // $this->impressos_categorias = new Application_Model_Db_ImpressosCategorias();
        // $this->cores = new Application_Model_Db_Cores();
        // $this->tamanhos = new Application_Model_Db_Tamanhos();
        // $this->voltagens = new Application_Model_Db_Voltagens();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        $this->select = new ZendPlugin_Db_Select(Zend_Db_Table::getDefaultAdapter());
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            if($post['search-by']=='categoria_id'){
                $rows = array();
                $select = $this->select->reset();
                $select->from('impressos as p',array('*'));
                $select->joinLeft('impressos_categorias as pc','pc.impresso_id = p.id',array());
                
                $where = 'pc.categoria_id in ('.$post['search-txt'].')';
                $select->where($where);
                $select->order('ordem');
                $select->group('p.id');
                $select->limit($limit,$offset);

                // _d($select->__toString());
                $_rows = $select->query()->fetchAll();
                foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));

                // count
                $select_count = $this->select->reset();
                $select_count->from('impressos as p',array('count(p.id) as cnt'));
                $select_count->joinLeft('impressos_categorias as pc','pc.impresso_id = p.id',array());
                $select_count->where($where);
                // $select_count->group('p.id');
                $row_total = $select_count->query()->fetchAll();
                $total = $this->view->total = $row_total[0]['cnt']; //_d($total);
            } else {
                $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
                $rows = $this->impressos->fetchAll($where,'ordem',$limit,$offset);
                $rows = Is_Array::utf8DbResult($rows);
                
                $total = $this->view->total = $this->impressos->count($where);
            }
        } else {
            $rows = $this->impressos->fetchAll(null,"ordem",$limit,$offset);
            $rows = Is_Array::utf8DbResult($rows);
            $total = $this->view->total = $this->impressos->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $form = new Admin_Form_Impressos();
        // $this->view->categorias = $form->getElement('categoria_id')->options;
        $this->view->categorias = null;//$this->categorias->getParentsKV($this->categorias->getWithChildren());
        $this->view->roles = $this->impressos->getRoles();
        $this->view->rows = $rows;
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Impressos();
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            $data['peso'] = !strstr($data['peso'],',') && strlen(trim($data['peso'])) > 0 ?
                                $this->view->currency($data['peso'],array('display'=>Zend_Currency::NO_SYMBOL)) :
                                $data['peso'];
            $data['valor'] = !strstr($data['valor'],',') && strlen(trim($data['valor'])) > 0 ?
                                $this->view->currency($data['valor'],array('display'=>Zend_Currency::NO_SYMBOL)) :
                                $data['valor'];
            $data['valor_promo'] = !strstr($data['valor_promo'],',') && strlen(trim($data['valor_promo'])) > 0 ?
                                $this->view->currency($data['valor_promo'],array('display'=>Zend_Currency::NO_SYMBOL)) :
                                $data['valor_promo'];
            $this->view->id = $this->impresso_id = $data['id'];
            $this->view->alias = $this->impresso_alias = $data['alias'];

            // categorias
            $this->view->categoria_id = array();
            
            if(isset($data['id'])){
                $categorias = array();//$this->impressos_categorias->fetchAll('impresso_id='.$data['id']);

                if(count($categorias)){
                    foreach($categorias as $cat){
                        $this->view->categoria_id[] = $cat->categoria_id;
                    }
                }
            }

            if(isset($data['categoria_id'])) unset($data['categoria_id']);
            
            // outros campos
            $form->addElement('hidden','id');
            $this->view->fotos = $this->fotosAction();
            // $this->view->sugestoes = $this->sugestoesAction();
            // $this->view->sugestoes_list = $this->impressos->getSugestoes($data['id']);
            // $this->view->tamanhos = $this->tamanhosAction();
            // $this->view->tamanhos_list = $this->impressos->getTamanhos($data['id']);
            // $this->view->voltagens = $this->voltagensAction();
            // $this->view->voltagens_list = $this->impressos->getVoltagens($data['id']);
            // $this->view->cores = $this->coresAction();
            // $this->view->cores_list = $this->impressos->getCores($data['id']);
            unset($data['info']);
            
            // checagem de estoque
            /*if($data['estoque'] < 1){
                $this->messenger->addMessage('Este impresso está sem estoque.','error');
            } else if($data['estoque'] <= $data['estoque_minimo']){
                $this->messenger->addMessage('O estoque deste impresso está se esgotando.','error');
            }*/
        } else {
            $data = array('status_id'=>'1');
            
            if(isset($_SERVER['HTTP_REFERER'])){
                if(strstr($_SERVER['HTTP_REFERER'],'categoria_id') && strstr($_SERVER['HTTP_REFERER'],VALEPRESENTEID)){
                    $data['categoria_id'] = VALEPRESENTEID;
                }
            }
        }
        
        $form->populate($data);
        $this->view->form = $form;
        
        //$this->view->cores = array_merge(array('0'=>'Cor...'),$this->cores->getKeyValues('descricao',false,'ordem'));
        // $this->view->cores = $this->cores->getKeyValues('descricao',array('0'=>'Cor...'),'ordem');
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/impressos/new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->impressos->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();

            $categoria_id = $post['categoria_id'];
            unset($post['categoria_id']);

            $data = $post;
            $data['alias']       = Is_Str::toUrl($post['titulo'].((bool)$post['cod'] ? ' '.$post['cod'] : ''));
            $data['descricao']   = cleanHtml($data['descricao'],'<b><a><i><u><br><ul><ol><li><img><p><div>');
            $data['descricao_completa']   = cleanHtml($data['descricao_completa'],'<b><a><i><u><br><ul><ol><li><img><p><div>');
            // $data['medida_c']    = str_replace(',','.',$data['medida_c']);
            // $data['medida_l']    = str_replace(',','.',$data['medida_l']);
            // $data['medida_a']    = str_replace(',','.',$data['medida_a']);
            $data['peso']        = str_replace(',','.',$data['peso']);
            $data['valor']       = str_replace(',','.',$data['valor']);
            $data['valor_promo'] = str_replace(',','.',$data['valor_promo']);
            $data['user_edit']   = $this->login->user->id;
            $data['data_edit']   = date("Y-m-d H:i:s");
            $data = array_map('utf8_decode',$data);
            
            if(!$row){
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
            }

            // remove dados desnecessários
            $unsets = 'submit,module,controller,action,categoria_id';
            foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
            
            // grava impresso no banco
            ($row) ? $this->impressos->update($data,'id='.$id) : $id = $this->impressos->insert($data);

            // insere categorias
            if(count($categoria_id)){
                $data_cat = array();

                foreach($categoria_id as $cat_id){
                    $data_cat[] = array(
                        'categoria_id' => $cat_id,
                        'impresso_id'   => $id
                    );
                }

                try {
                    $this->impressos_categorias->delete('impresso_id='.$id);
                    $this->impressos_categorias->insertAll($data_cat);
                } catch(Exception $e) {
                    $err = strstr($e->getMessage(),'uplicate') ? 'Você não pode inserir duas categorias iguais para o mesmo impresso.' : 'Erro ao salvar categorias.';
                    if(APPLICATION_ENV!='production') $err.= '<br>'.$e->getMessage();
                    $this->messenger->addMessage($err,'error');
                }
            }

            // avise-me
            $avisemes = array();//$this->impressos_aviseme->fetchAll('impresso_id='.$id);

            if(count($avisemes) && (int)$data['estoque'] > 0){
                $subj = 'Impresso disponível - '.SITE_TITLE;
                $html = '<h1>Olá [:NOME:], </h1>'.
                        '<p>O impresso <b>"'.utf8_encode($data['titulo']).'"</b> está disponível novamente em nosso site!</p>'.
                        '<p>Não perca tempo e verifique <a href="'.URL.'/impresso/'.$data['alias'].'/">clicando aqui!</a></p>'.
                        '<p>Boas compras!</p><br><br>';

                try {
                    foreach($avisemes as $aviseme){
                        $html = str_replace('[:NOME:]',utf8_encode($aviseme->nome),$html);
                        Trupe_Casadocristal_Mail::send($aviseme->email,$aviseme->nome,$subj,$html);
                    }

                    $this->impressos_aviseme->delete('impresso_id='.$id);
                } catch(Exception $e) {
                    $err = 'Erro ao enviar e-mails de aviso para clientes.';
                    if(APPLICATION_ENV!='production') $err.= ' - '.$e->getMessage();
                    $this->messenger->addMessage($err,'error');
                }
            }
            
            // ok, redirecionando
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $data = Is_Array::utf8All($data);
            $data['valor'] = str_replace('.',',',$data['valor']);
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>$data));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Já existe um impresso com o mesmo título e peso, escolha um diferente.' :
                     $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->impressos->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('denied','error','default',array('url'=>URL.'/admin/impressos/'));return false; }
        
        $data = Is_Array::utf8All($row->toArray());
        $row_info = null;//$this->impressos_infos->fetchRow('impresso_id='.$data['id']);
        $data['info'] = $row_info ? Is_Array::utf8All($row_info->toArray()) : NULL;
        //Is_Var::dump($data);
        $this->_forward('new',null,null,array('data'=>$data));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Impressos();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function sugestoesAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('impressos')/*->where('categoria_id=30')*/->order('descricao')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }
    
    public function sugestoesAddAction()
    {
        if($this->_hasParam('impresso_id') && $this->_hasParam('sugestao_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressosSugestoes();
            try{
                $id = $impressos_sugestoes->insert(array(
                    'impresso_id'=>$this->_getParam('impresso_id'),
                    'sugestao_id'=>$this->_getParam('sugestao_id')
                ));
                return array('id' => $id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Relacionamento já inserido." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar registro.');
        }
    }
    
    public function sugestoesDelAction()
    {
        if($this->_hasParam('impresso_id') && $this->_hasParam('sugestao_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressosSugestoes();
            $impressos_sugestoes->delete('impresso_id="'.$this->_getParam('impresso_id').'"'.
                                        'and sugestao_id="'.$this->_getParam('sugestao_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover registro.');
        }
    }

    /**
     * Salva ordenação de fotos via ajax
     */
    public function ordemFotosAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            if(!@$this->fotos)
                $this->fotos = new Application_Model_Db_Fotos();

            for($i=0;$i<sizeof($values['id']);$i++) $this->fotos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('impressos_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f.ordem');
            // ->order('f2.id asc');
        
        if(isset($this->impresso_id)){
            $select->where('f2.impresso_id = ?',$this->impresso_id);
        }
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        // adicionando alias ao foto->path
        if(0 && count($fotos)) foreach($fotos as $f) {
            $alias = '';
            if((bool)trim($f->titulo)) $alias = Is_Str::toUrl($f->titulo).'-';
            else if(isset($this->impresso_alias)) $alias = $this->impresso_alias.'-';
            $f->path = $alias.$f->path;
        }
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            Is_File::del($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        $max_size = ini_get('post_max_size').'B'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/impressos/'));
            return;
        }
        
        $file = $_FILES['file'];
        $filename = Is_File::getName($file['name']);
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $impressos_fotos = new Application_Model_Db_ImpressosFotos();
            $impresso_id = $this->_getParam('id');
            
            $data_fotos = array(
                "titulo"   => $filename,
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $impressos_fotos->insert(array("foto_id"=>$foto_id,"impresso_id"=>$impresso_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function fotoCorAction()
    {
        $impresso = $this->_getParam("impresso");
        $foto = $this->_getParam("foto");
        $cor  = $this->_getParam("cor");
        
        if(!(bool)$foto || !(bool)$cor) return array("erro"=>'Erro ao enviar');
        
        $fotos = new Application_Model_Db_ImpressosFotos();
        
        try {
            $fotos->update(array('cor_id'=>$cor),"impresso_id=".$impresso.' and foto_id='.$foto);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }

    /* tamanho actions */
    public function tamanhosAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('tamanhos')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function tamanhosAddAction()
    {
        if($this->_hasParam('impresso_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressoTamanho();
            try{
                $tamanho_id = $this->_getParam('tamanho_id');

                if($this->_hasParam('tamanho_descricao')){
                    $tamanho_descricao = trim($this->_getParam('tamanho_descricao'));
                    $tamanho_alias = Is_Str::toUrl(trim($tamanho_descricao));

                    if($rowtamanho = $this->tamanhos->fetchRow('alias="'.$tamanho_alias.'"')){
                        $tamanho_id = $rowtamanho->id;
                    } else {
                        $tamanho_data = array(
                            'descricao' => (utf8_decode($tamanho_descricao)),
                            'alias' => $tamanho_alias
                        );
                        $tamanho_id = $this->tamanhos->insert($tamanho_data);
                    }

                }

                $id = $impressos_sugestoes->insert(array(
                    'impresso_id'=>$this->_getParam('impresso_id'),
                    'tamanho_id'=>$tamanho_id
                ));
                return array('id' => $tamanho_id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar o tamanho.');
        }
    }
    
    public function tamanhosDelAction()
    {
        if($this->_hasParam('impresso_id') && $this->_hasParam('tamanho_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressoTamanho();
            $impressos_sugestoes->delete('impresso_id="'.$this->_getParam('impresso_id').'"'.
                                        'and tamanho_id="'.$this->_getParam('tamanho_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover o tamanho.');
        }
    }

    /* voltagens actions */
    public function voltagensAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('voltagens')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function voltagensAddAction()
    {
        if($this->_hasParam('impresso_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressoVoltagem();
            try{
                $voltagem_id = $this->_getParam('voltagem_id');

                if($this->_hasParam('voltagem_descricao')){
                    $voltagem_descricao = trim($this->_getParam('voltagem_descricao'));
                    $voltagem_alias = Is_Str::toUrl(trim($voltagem_descricao));

                    if($rowvoltagem = $this->voltagens->fetchRow('alias="'.$voltagem_alias.'"')){
                        $voltagem_id = $rowvoltagem->id;
                    } else {
                        $voltagem_data = array(
                            'descricao' => (utf8_decode($voltagem_descricao)),
                            'alias' => $voltagem_alias
                        );
                        $voltagem_id = $this->voltagens->insert($voltagem_data);
                    }

                }

                $id = $impressos_sugestoes->insert(array(
                    'impresso_id'=>$this->_getParam('impresso_id'),
                    'voltagem_id'=>$voltagem_id
                ));
                return array('id' => $voltagem_id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar a voltagem.');
        }
    }
    
    public function voltagensDelAction()
    {
        if($this->_hasParam('impresso_id') && $this->_hasParam('voltagem_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressoVoltagem();
            $impressos_sugestoes->delete('impresso_id="'.$this->_getParam('impresso_id').'"'.
                                        'and voltagem_id="'.$this->_getParam('voltagem_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover a voltagem.');
        }
    }

    /* cores actions */
    public function coresAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('cores')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function coresAddAction()
    {
        if($this->_hasParam('impresso_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressoCor();
            try{
                $cor_id = $this->_getParam('cor_id');

                if($this->_hasParam('cor_descricao')){
                    $cor_descricao = trim($this->_getParam('cor_descricao'));
                    $cor_alias = Is_Str::toUrl(trim($cor_descricao));

                    if($rowcor = $this->cores->fetchRow('alias="'.$cor_alias.'"')){
                        $cor_id = $rowcor->id;
                    } else {
                        $cor_data = array(
                            'descricao' => (utf8_decode($cor_descricao)),
                            'alias' => $cor_alias
                        );
                        $cor_id = $this->cores->insert($cor_data);
                    }

                }

                $id = $impressos_sugestoes->insert(array(
                    'impresso_id'=>$this->_getParam('impresso_id'),
                    'cor_id'=>$cor_id
                ));
                return array('id' => $cor_id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar a cor.');
        }
    }
    
    public function coresDelAction()
    {
        if($this->_hasParam('impresso_id') && $this->_hasParam('cor_id')){
            $impressos_sugestoes = new Application_Model_Db_ImpressoCor();
            $impressos_sugestoes->delete('impresso_id="'.$this->_getParam('impresso_id').'"'.
                                        'and cor_id="'.$this->_getParam('cor_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover a cor.');
        }
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->impressos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}
