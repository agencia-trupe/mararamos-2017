<?php

class Admin_ProdutosController extends ZendPlugin_Controller_Ajax
{
    protected $_require_db = array(
        'produtos' => 'Produtos',
    );

    public function init()
    {
        Application_Model_Login::checkAuth($this);
        
        $this->view->titulo = "PROJETOS";

        $tipo = ($this->_hasParam('tipo')) ? $this->_getParam('tipo') : 1;
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            if(@$data['tipo']) $tipo = $data['tipo'];
        }
        $this->view->tipo = $this->tipo = $tipo;
        if($tipo) $this->view->titulo = $this->produtos->getTipos($tipo);

        $this->view->section = $this->section = "produtos";
        $this->view->url = $this->_url = $this->_request->getBaseUrl()."/admin/".$this->section."/";
        $this->view->titulo = "<a href='".$this->_url."?tipo=".$this->tipo."'>".$this->view->titulo."</a>";
        $this->img_path  = $this->view->img_path  = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".IMG_PATH."/".$this->section;
        $this->file_path = $this->view->file_path = APPLICATION_PATH."/../".SCRIPT_RETURN_PATH."/".FILE_PATH."/".$this->section;
        
        $this->view->MAX_FOTOS = 0;
        
        // models
        $this->categorias = new Application_Model_Db_Categorias();
        $this->produtos_infos = new Application_Model_Db_ProdutosInfos();
        // $this->produtos_aviseme = new Application_Model_Db_ProdutosAviseme();
        $this->produtos_categorias = new Application_Model_Db_ProdutosCategorias();
        $this->cores = new Application_Model_Db_Cores();
        // $this->tamanhos = new Application_Model_Db_Tamanhos();
        // $this->voltagens = new Application_Model_Db_Voltagens();
        $this->login = new Zend_Session_Namespace(SITE_NAME.'_login');
        $this->messenger = new Helper_Messenger();
        $this->select = new ZendPlugin_Db_Select(Zend_Db_Table::getDefaultAdapter());
    }

    public function indexAction()
    {
        /* paginação */
        $records_per_page   = 20;
        $selectable_pages   = 15;
        $pagination = new Php_Zebra_Pagination();
        $limit  = $records_per_page;
        $offset = (($pagination->get_page() - 1) * $records_per_page);
        
        if($this->_hasParam('search-by')){
            $post = $_POST = $this->_request->getParams();
            
            if($post['search-by']=='categoria_id'){
                $rows = array();
                $select = $this->select->reset();
                $select->from('produtos as p',array('p.*'));
                $select->joinLeft('produtos_categorias as pc','pc.produto_id = p.id',array());
                
                $where = 'pc.categoria_id in ('.$post['search-txt'].') ';
                $where.= 'and p.tipo = "'.$this->tipo.'" ';
                $select->where($where);
                $select->order('ordem');
                $select->group('p.id');
                $select->limit($limit,$offset);

                // _d($select->__toString());
                $_rows = $select->query()->fetchAll();
                foreach($_rows as $k=>&$v) $rows[] = Is_Array::toObject(Is_Array::utf8All($v));

                // count
                $select_count = $this->select->reset();
                $select_count->from('produtos as p',array('count(p.id) as cnt'));
                $select_count->joinLeft('produtos_categorias as pc','pc.produto_id = p.id',array());
                $select_count->where($where);
                // $select_count->group('p.id');
                $row_total = $select_count->query()->fetchAll();
                $total = $this->view->total = $row_total[0]['cnt']; //_d($total);
            } else {
                $where = $post['search-by']." like '%".utf8_decode(str_replace(" ","%",$post['search-txt']))."%'";
                $where.= 'and tipo = "'.$this->tipo.'" ';
                $rows = $this->produtos->fetchAll($where,'ordem',$limit,$offset);
                $rows = Is_Array::utf8DbResult($rows);
                
                $total = $this->view->total = $this->produtos->count($where);
            }
        } else {
            $where = '1=1 ';
            $where.= 'and tipo = "'.$this->tipo.'" ';
            $rows = $this->produtos->fetchAll($where,"ordem",$limit,$offset);
            $rows = Is_Array::utf8DbResult($rows);
            // _d($rows);
            $total = $this->view->total = $this->produtos->count();
        }
        
        /* seta parâmetros da paginação */
        $pagination->records($total)
                   ->records_per_page($records_per_page)
                   ->selectable_pages($selectable_pages)
                   ->padding(false);
        
        $this->view->paginacao = $pagination;
        
        $form = new Admin_Form_Produtos(null,$this->tipo);
        // $this->view->categorias = $form->getElement('categoria_id')->options;
        $this->view->categorias = $this->categorias->getParentsKV($this->categorias->getWithChildren());
        $this->view->roles = $this->produtos->getRoles();
        $this->view->rows = $rows;
    }
    
    public function newAction()
    {
        $this->view->titulo = $this->view->titulo.($this->_hasParam('data')?" &rarr; EDITAR":" &rarr; INSERIR");
        $form = new Admin_Form_Produtos(null,$this->tipo);
        
        if($this->_hasParam('data')){
            $data = $this->_getParam('data');
            // $data['peso'] = !strstr($data['peso'],',') && strlen(trim($data['peso'])) > 0 ?
            //                     $this->view->currency($data['peso'],array('display'=>Zend_Currency::NO_SYMBOL)) :
            //                     $data['peso'];
            // $data['valor'] = !strstr($data['valor'],',') && strlen(trim($data['valor'])) > 0 ?
            //                     $this->view->currency($data['valor'],array('display'=>Zend_Currency::NO_SYMBOL)) :
            //                     $data['valor'];
            // $data['valor_promo'] = !strstr($data['valor_promo'],',') && strlen(trim($data['valor_promo'])) > 0 ?
            //                     $this->view->currency($data['valor_promo'],array('display'=>Zend_Currency::NO_SYMBOL)) :
            //                     $data['valor_promo'];
            $this->view->id = $this->produto_id = $data['id'];
            $this->view->alias = $this->produto_alias = @$data['alias'];
            $row = $this->row = (object)$data;
            $this->view->row = $row;
            $this->view->produto = $row;

            // categorias
            $this->view->categoria_id = array();
            
            if(isset($data['id'])){
                $categorias = $this->produtos_categorias->fetchAll('produto_id='.$data['id']);

                if(count($categorias)){
                    foreach($categorias as $cat){
                        $this->view->categoria_id[] = $cat->categoria_id;
                    }
                }
            }

            if(isset($data['categoria_id'])) unset($data['categoria_id']);
            
            // outros campos
            $form->addElement('hidden','id');
            $this->view->fotos = $this->fotosAction();
            $this->fotosFixasAction();
            // $this->view->sugestoes = $this->sugestoesAction();
            // $this->view->sugestoes_list = $this->produtos->getSugestoes($data['id']);
            // $this->view->tamanhos = $this->tamanhosAction();
            // $this->view->tamanhos_list = $this->produtos->getTamanhos($data['id']);
            // $this->view->voltagens = $this->voltagensAction();
            // $this->view->voltagens_list = $this->produtos->getVoltagens($data['id']);
            // $this->view->cores = $this->coresAction();
            // $this->view->cores_list = $this->produtos->getCores($data['id']);
            unset($data['info']);
            
            // checagem de estoque
            /*if($data['estoque'] < 1){
                $this->messenger->addMessage('Este produto está sem estoque.','error');
            } else if($data['estoque'] <= $data['estoque_minimo']){
                $this->messenger->addMessage('O estoque deste produto está se esgotando.','error');
            }*/
        } else {
            $data = array('status_id'=>'1','tipo'=>$this->tipo);
            
            if(isset($_SERVER['HTTP_REFERER'])){
                if(strstr($_SERVER['HTTP_REFERER'],'categoria_id') && strstr($_SERVER['HTTP_REFERER'],VALEPRESENTEID)){
                    $data['categoria_id'] = VALEPRESENTEID;
                }
            }
        }
        
        $form->populate($data);
        $this->view->form = $form;
        
        //$this->view->cores = array_merge(array('0'=>'Cor...'),$this->cores->getKeyValues('descricao',false,'ordem'));
        // $this->view->cores = $this->cores->getKeyValues('descricao',array('0'=>'Cor...'),'ordem');
    }
    
    public function saveAction()
    {
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/new/'));
            return;
        }
        
        $id = (int)$this->_getParam("id");
        $row = $this->produtos->fetchRow('id='.$id); // verifica registro
        
        try {
            // define dados
            $post = $this->_request->getParams();

            $categoria_id = @$post['categoria_id'];
            if(isset($post['categoria_id'])) unset($post['categoria_id']);

            $data = $post;
            $data['alias']       = Is_Str::toUrl($post['titulo'].((bool)@$post['cod'] ? ' '.$post['cod'] : ''));
            $data['descricao']   = cleanHtml($data['descricao'],'<b><a><i><u><br><ul><ol><li><img><p><div>');
            // $data['descricao_completa']   = cleanHtml($data['descricao_completa'],'<b><a><i><u><br><ul><ol><li><img><p><div>');
            // $data['medida_c']    = str_replace(',','.',$data['medida_c']);
            // $data['medida_l']    = str_replace(',','.',$data['medida_l']);
            // $data['medida_a']    = str_replace(',','.',$data['medida_a']);
            // $data['peso']        = str_replace(',','.',$data['peso']);
            // $data['valor']       = str_replace(',','.',$data['valor']);
            // $data['valor_promo'] = str_replace(',','.',$data['valor_promo']);
            $data['user_edit']   = $this->login->user->id;
            $data['data_edit']   = date("Y-m-d H:i:s");
            $data = array_map('utf8_decode',$data);
            
            if(!$row){
                $data['user_cad']  = $this->login->user->id;
                $data['data_cad']  = date("Y-m-d H:i:s");
            }

            // remove dados desnecessários
            $unsets = 'submit,module,controller,action,categoria_id';
            foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
            
            // grava produto no banco
            ($row) ? $this->produtos->update($data,'id='.$id) : $id = $this->produtos->insert($data);

            // insere categorias
            if(count($categoria_id)){
                $data_cat = array();

                foreach($categoria_id as $cat_id){
                    $data_cat[] = array(
                        'categoria_id' => $cat_id,
                        'produto_id'   => $id
                    );
                }

                try {
                    $this->produtos_categorias->delete('produto_id='.$id);
                    $this->produtos_categorias->insertAll($data_cat);
                } catch(Exception $e) {
                    $err = strstr($e->getMessage(),'uplicate') ? 'Você não pode inserir duas categorias iguais para o mesmo produto.' : 'Erro ao salvar categorias.';
                    if(APPLICATION_ENV!='production') $err.= '<br>'.$e->getMessage();
                    $this->messenger->addMessage($err,'error');
                }
            }

            // avise-me
            $avisemes = array();//$this->produtos_aviseme->fetchAll('produto_id='.$id);

            if(count($avisemes) && (int)$data['estoque'] > 0){
                $subj = 'Produto disponível - '.SITE_TITLE;
                $html = '<h1>Olá [:NOME:], </h1>'.
                        '<p>O produto <b>"'.utf8_encode($data['titulo']).'"</b> está disponível novamente em nosso site!</p>'.
                        '<p>Não perca tempo e verifique <a href="'.URL.'/produto/'.$data['alias'].'/">clicando aqui!</a></p>'.
                        '<p>Boas compras!</p><br><br>';

                try {
                    foreach($avisemes as $aviseme){
                        $html = str_replace('[:NOME:]',utf8_encode($aviseme->nome),$html);
                        Trupe_Casadocristal_Mail::send($aviseme->email,$aviseme->nome,$subj,$html);
                    }

                    $this->produtos_aviseme->delete('produto_id='.$id);
                } catch(Exception $e) {
                    $err = 'Erro ao enviar e-mails de aviso para clientes.';
                    if(APPLICATION_ENV!='production') $err.= ' - '.$e->getMessage();
                    $this->messenger->addMessage($err,'error');
                }
            }
            
            // ok, redirecionando
            $this->messenger->addMessage('Registro atualizado.');
            $data['id'] = $id;
            $data = Is_Array::utf8All($data);
            $data['valor'] = str_replace('.',',',$data['valor']);
            $this->_redirect('admin/'.$this->section.'/edit/'.$id.'/');
            //$this->_forward('new',null,null,array('data'=>$data));
        } catch(Exception $e) {
            $error = strstr($e->getMessage(),'uplicate') ?
                     'Já existe um produto com o mesmo título e peso, escolha um diferente.' :
                     $e->getMessage();
            $this->messenger->addMessage($error,'error');
            $this->_forward('new',null,null,array('data'=>$this->_request->getParams()));
        }
    }
    
    public function editAction()
    {
        $id    = (int)$this->_getParam('id');
        $row   = $this->produtos->fetchRow('id='.$id);
        
        if(!$row){ $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/'));return false; }
        
        $data = Is_Array::utf8All($row->toArray());
        $row_info = $this->produtos_infos->fetchRow('produto_id='.$data['id']);
        $data['info'] = $row_info ? Is_Array::utf8All($row_info->toArray()) : NULL;
        //Is_Var::dump($data);
        $this->_forward('new',null,null,array('data'=>$data));
    }
    
    public function delAction(){
        $id = $this->_getParam("id");
        $table = new Application_Model_Db_Produtos();
        
        try {
            $table->delete("id=".(int)$id);
            return array();
        } catch(Exception $e) {
            return array("erro"=>"Erro ao excluir registro.");
        }
    }
    
    public function sugestoesAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('produtos')/*->where('categoria_id=30')*/->order('descricao')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }
    
    public function sugestoesAddAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('sugestao_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutosSugestoes();
            try{
                $id = $produtos_sugestoes->insert(array(
                    'produto_id'=>$this->_getParam('produto_id'),
                    'sugestao_id'=>$this->_getParam('sugestao_id')
                ));
                return array('id' => $id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Relacionamento já inserido." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar registro.');
        }
    }
    
    public function sugestoesDelAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('sugestao_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutosSugestoes();
            $produtos_sugestoes->delete('produto_id="'.$this->_getParam('produto_id').'"'.
                                        'and sugestao_id="'.$this->_getParam('sugestao_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover registro.');
        }
    }

    /**
     * Salva ordenação de fotos via ajax
     */
    public function ordemFotosAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            if(!@$this->fotos)
                $this->fotos = new Application_Model_Db_Fotos();

            for($i=0;$i<sizeof($values['id']);$i++) $this->fotos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }

    public function fotosFixasAction()
    {
        if(!@$this->row && !@$this->produto_id) {
            $this->view->foto_fixa = null;
            return null;
        }
        if(!@$this->row) $this->row = $this->produtos->get($this->produto_id);
        if(!@$this->row) {
            $this->view->foto_fixa = null;
            return null;
        }

        $fotos = $this->produtos->s1('fotos','*','id = "'.$this->row->foto_id.'"');
        
        // _d($fotos);
        $this->view->foto_fixa = $fotos;
    }

    public function saveFotosFixasAction()
    {
        if(!$this->_hasParam('fotos_fixas_id')) return $this->_forward('denied','error','default',array('url'=>URL.'/admin/'));
        
        $post = $this->_request->getParams();
        //$data_resposta = array_map('utf8_decode',$post['question']);
        //unset($post['question']);
        $data = array_map('utf8_decode',$post);
        $fotos = array();
        
        // remove dados desnecessários
        $unsets = 'submit,module,controller,action,fotos,enviar';
        foreach(explode(',',$unsets) as $u) if(isset($data[$u])) unset($data[$u]);
        
        // upload de arquivos
        $file = null; $rename = null;
        $table = new Application_Model_Db_Fotos();
        
        // $check_uploads = array();
        // for($i=1;$i<=$data['fotos_fixas_qtde'];$i++) $check_uploads[] = 'foto'.$i;
        // $check_uploads = implode(',',$check_uploads);
        $check_uploads = 'foto_id';

        foreach(explode(',',$check_uploads) as $cu)
            if((bool)@$_FILES[$cu]) 
                if(!(bool)$_FILES[$cu]['name']) 
                    unset($_FILES[$cu]);
        
        foreach(explode(',',$check_uploads) as $cu) {
            if((bool)@$_FILES[$cu] && (bool)$_FILES[$cu]['name']){
                $file = $_FILES[$cu];
                $path = $this->img_path;
                $error = 'Imagem inválida';
                $ext = 'jpg,jpeg,png,bmp,gif,tiff';
            
                $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->addValidator('Size', false, array('max' => '50mb'))
                       ->addValidator('Extension', false, $ext)
                       ->addFilter('Rename',$path.'/'.$rename,$cu)
                       ->setDestination($path);

                if($upload->isValid($cu)){
                    $upload->receive($cu);

                    $fotos[$cu] = $table->insert(array('path'=>$rename));
                } else {
                    $err = $error;
                    if(APPLICATION_ENV!='production') $err.= "<br/>\n".var_dump($upload->getErrors());
                    $this->messenger->addMessage($err,'error');
                    
                    $this->_redirect('admin/produtos/edit/'.$data['produto_id']);
                }
            }
        }
        
        if(count($fotos)) {
            $this->produtos->update($fotos,'id = '.$data['produto_id']);
        }

        $this->messenger->addMessage('Fotos enviadas com sucesso');
        return $this->_redirect('admin/produtos/edit/'.$data['produto_id']);
    }
    
    public function fotosAction()
    {
        //$this->view->titulo.= " &rarr; FOTOS";
        
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('produtos_fotos as f2')
            ->join('fotos as f','f.id=f2.foto_id')
            ->order('f.ordem');
            // ->order('f2.id asc');
        
        if(isset($this->produto_id)){
            $select->where('f2.produto_id = ?',$this->produto_id);
        }
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        // adicionando alias ao foto->path
        if(0 && count($fotos)) foreach($fotos as $f) {
            $alias = '';
            if((bool)trim($f->titulo)) $alias = Is_Str::toUrl($f->titulo).'-';
            else if(isset($this->produto_alias)) $alias = $this->produto_alias.'-';
            $f->path = $alias.$f->path;
        }
        
        return $fotos;
    }
    
    public function fotosDelAction()
    {
        $id = $this->_getParam("file");
        $fotos = new Application_Model_Db_Fotos();
        $foto = $fotos->fetchRow('id='.(int)$id);
                
        try {
            $fotos->delete("id=".(int)$id);
            Is_File::delDerived($this->img_path.'/'.$foto->path);
            Is_File::del($this->img_path.'/'.$foto->path);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }
    
    public function uploadAction()
    {
        //echo $this->img_path;exit();
        $max_size = ini_get('post_max_size').'B'; //'5120'; //'2048';
        
        if(!$this->_request->isPost()){
            $this->_forward('denied','error','default',array('url'=>URL.'/admin/produtos/'));
            return;
        }
        
        $file = $_FILES['file'];
        $filename = Is_File::getName($file['name']);
        $rename = Is_File::getRandomName().'.'.Is_File::getExt($file['name']);
        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->addValidator('Extension', false, 'jpeg,jpg,png,gif,bmp')
               ->addValidator('Size', false, array('max' => $max_size))
               ->addValidator('Count', false, 1)
               ->addFilter('Rename',$this->img_path.'/'.$rename)
               ->setDestination($this->img_path);
        
        if(!$upload->isValid()){
            return array('error'=>'Erro: o arquivo tem que ser uma imagem válida de até '.$max_size.'.');
        }
        
        try {
            $upload->receive();
            
            $thumb = Php_Thumb_Factory::create($this->img_path.'/'.$rename);
            $thumb->resize('1000','1000');
            $thumb->save($this->img_path.'/'.$rename);
            
            $fotos = new Application_Model_Db_Fotos();
            $produtos_fotos = new Application_Model_Db_ProdutosFotos();
            $produto_id = $this->_getParam('id');
            
            $data_fotos = array(
                "titulo"   => $filename,
                "path"     => $rename,
                "user_cad" => $this->login->user->id,
                "data_cad" => date("Y-m-d H:i:s")
            );
            
            if(!$foto_id = $fotos->insert($data_fotos)){
                return array('error'=>'Erro ao inserir arquivo no banco de dados.');
            }
            $produtos_fotos->insert(array("foto_id"=>$foto_id,"produto_id"=>$produto_id));
            
            return array("name"=>$rename,"id"=>$foto_id);
        } catch (Exception $e)  {
            return array('error'=>$e->getMessage());
        }
        
        exit();
    }
    
    public function fotoCorAction()
    {
        $produto = $this->_getParam("produto");
        $foto = $this->_getParam("foto");
        $cor  = $this->_getParam("cor");
        
        if(!(bool)$foto || !(bool)$cor) return array("erro"=>'Erro ao enviar');
        
        $fotos = new Application_Model_Db_ProdutosFotos();
        
        try {
            $fotos->update(array('cor_id'=>$cor),"produto_id=".$produto.' and foto_id='.$foto);
            return array();
        } catch(Exception $e) {
            return array("erro"=>$e->getMessage());
        }
    }

    /* tamanho actions */
    public function tamanhosAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('tamanhos')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function tamanhosAddAction()
    {
        if($this->_hasParam('produto_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoTamanho();
            try{
                $tamanho_id = $this->_getParam('tamanho_id');

                if($this->_hasParam('tamanho_descricao')){
                    $tamanho_descricao = trim($this->_getParam('tamanho_descricao'));
                    $tamanho_alias = Is_Str::toUrl(trim($tamanho_descricao));

                    if($rowtamanho = $this->tamanhos->fetchRow('alias="'.$tamanho_alias.'"')){
                        $tamanho_id = $rowtamanho->id;
                    } else {
                        $tamanho_data = array(
                            'descricao' => (utf8_decode($tamanho_descricao)),
                            'alias' => $tamanho_alias
                        );
                        $tamanho_id = $this->tamanhos->insert($tamanho_data);
                    }

                }

                $id = $produtos_sugestoes->insert(array(
                    'produto_id'=>$this->_getParam('produto_id'),
                    'tamanho_id'=>$tamanho_id
                ));
                return array('id' => $tamanho_id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar o tamanho.');
        }
    }
    
    public function tamanhosDelAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('tamanho_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoTamanho();
            $produtos_sugestoes->delete('produto_id="'.$this->_getParam('produto_id').'"'.
                                        'and tamanho_id="'.$this->_getParam('tamanho_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover o tamanho.');
        }
    }

    /* voltagens actions */
    public function voltagensAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('voltagens')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function voltagensAddAction()
    {
        if($this->_hasParam('produto_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoVoltagem();
            try{
                $voltagem_id = $this->_getParam('voltagem_id');

                if($this->_hasParam('voltagem_descricao')){
                    $voltagem_descricao = trim($this->_getParam('voltagem_descricao'));
                    $voltagem_alias = Is_Str::toUrl(trim($voltagem_descricao));

                    if($rowvoltagem = $this->voltagens->fetchRow('alias="'.$voltagem_alias.'"')){
                        $voltagem_id = $rowvoltagem->id;
                    } else {
                        $voltagem_data = array(
                            'descricao' => (utf8_decode($voltagem_descricao)),
                            'alias' => $voltagem_alias
                        );
                        $voltagem_id = $this->voltagens->insert($voltagem_data);
                    }

                }

                $id = $produtos_sugestoes->insert(array(
                    'produto_id'=>$this->_getParam('produto_id'),
                    'voltagem_id'=>$voltagem_id
                ));
                return array('id' => $voltagem_id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar a voltagem.');
        }
    }
    
    public function voltagensDelAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('voltagem_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoVoltagem();
            $produtos_sugestoes->delete('produto_id="'.$this->_getParam('produto_id').'"'.
                                        'and voltagem_id="'.$this->_getParam('voltagem_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover a voltagem.');
        }
    }

    /* cores actions */
    public function coresAction()
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $rows = $select->from('cores')/*->where('categoria_id=30')*/->order('alias')->query()->fetchAll();
        $rows = array_map('Is_Array::utf8All',$rows);
        return $rows;
    }

    public function coresAddAction()
    {
        if($this->_hasParam('produto_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoCor();
            try{
                $cor_id = $this->_getParam('cor_id');

                if($this->_hasParam('cor_descricao')){
                    $cor_descricao = trim($this->_getParam('cor_descricao'));
                    $cor_alias = Is_Str::toUrl(trim($cor_descricao));

                    if($rowcor = $this->cores->fetchRow('alias="'.$cor_alias.'"')){
                        $cor_id = $rowcor->id;
                    } else {
                        $cor_data = array(
                            'descricao' => (utf8_decode($cor_descricao)),
                            'alias' => $cor_alias
                        );
                        $cor_id = $this->cores->insert($cor_data);
                    }

                }

                $id = $produtos_sugestoes->insert(array(
                    'produto_id'=>$this->_getParam('produto_id'),
                    'cor_id'=>$cor_id
                ));
                return array('id' => $cor_id);
            } catch(Exception $e){
                $msg = strstr($e->getMessage(),"Duplicate") ? "Registro duplicado." : $e->getMessage();
                return array('error'=>$msg);
            }
        } else {
            return array('error'=>'Não foi possível adicionar a cor.');
        }
    }
    
    public function coresDelAction()
    {
        if($this->_hasParam('produto_id') && $this->_hasParam('cor_id')){
            $produtos_sugestoes = new Application_Model_Db_ProdutoCor();
            $produtos_sugestoes->delete('produto_id="'.$this->_getParam('produto_id').'"'.
                                        'and cor_id="'.$this->_getParam('cor_id').'"');
            return array();
        } else {
            return array('error'=>'Não foi possível remover a cor.');
        }
    }

    /**
     * Salva ordenação via ajax
     */
    public function ordemAction()
    {
        $values = array('id'=>$this->_getParam('id'),'ordem'=>$this->_getParam('ordem'));
        
        try {
            for($i=0;$i<sizeof($values['id']);$i++) $this->produtos->update(array(
                'ordem' => $values['ordem'][$i]
            ), 'id = "'.$values['id'][$i].'"');

            return array('msg'=>'Ordenação salva');
        } catch (Exception $e){
            return array('error'=>$e->getMessage());
        }
    }
    
    public function postDispatch()
    {
        $cm = $this->messenger->getCurrentMessages();
        $this->view->flash_messages = (bool)$cm ? $cm : $this->messenger->getMessages();
        //$this->view->flash_messages = $this->messenger->getCurrentMessages();
    }
}
