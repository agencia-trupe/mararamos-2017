<?php
/**
 * Funções genéricas
 */
class Func
{
	public function img($img)
	{
		return "<img src=\"".ROOT_PATH.$img."\" />";
	}
	
	public function removeDecorators(&$elm)
    {
        $elm->removeDecorator('label')
			->removeDecorator('htmlTag')
            ->removeDecorator('description')
            ->removeDecorator('errors');
    }
	
	public function clean($val)
	{
		return str_replace(array('.',',',' ','-','_','(',')','[',']'),'',trim($val));
	}
	
	public function br($j)
	{
		$br = "";
		for($i=0;$i<=$j;$i++){
			$br.= "<br/>";
		}
		return $br;
	}
    
    public static function _toUtf8(&$item, $key) {
        $item = iconv("iso-8859-1","utf-8",$item);
    }
    
    public static function _toIso(&$item, $key) {
        $item = iconv("utf-8","iso-8859-1",$item);
    }
    
    public function arrayToObject($array)
    {
        $object = new stdClass();
        if (is_array($array)){
            foreach ($array as $name=>$value){
                $name = trim($name);
                if (!empty($name)){
                    $object->$name = $value;
                }
            }
        } else {
            //die("param is not an array.");
        }
        return $object;
    }
    
    public function objectToArray($object)
    {
        $array = array();
        if (is_object($object)){
            $array = get_object_vars($object);
        }
        return $array;
    }
    
    public static function _encodeUtf8(&$item,$key)
	{
		$item = utf8_encode($item);
	}
    
    public static function _decodeUtf8(&$item,$key)
	{
		$item = utf8_decode($item);
	}
    
    public static function _arrayToObject(&$item,$key)
	{
		$item = self::arrayToObject($item);
	}
    
    public function drop($name=null,$belong,$options,$value='',$class='f1')
	{
		array_walk($options,'Func::_encodeUtf8');
		$drop = new Zend_Form_Element_Select($belong,$name ? array('belongsTo' => $name) : array());
		$drop->setAttrib('class',$class)
			->addMultiOptions($options)
			->setValue($value);
		Func::removeDecorators($drop);
		return $drop;
	}
    
    public function chk_cb($val1,$val2)
    {
        return $val1 == $val2 ? 'checked="checked"' : '';
    }
    
    public function chk_drop($val1,$val2)
    {
        return $val1 == $val2 ? 'selected="selected"' : '';
    }
}

class Img
{
    public function bg($path,$preload=false)
    {
        $path = URL."/img/".str_replace("/","|",$path);
        $style = "style=\"background-image:url('$path');\"";
        
        if($preload){
            $img = 'img_'.rand(0,999999);
            $script = Js::script("var $img = new Image(); $img.src = '$path';");
            return $style.'>'.substr($script,0,strlen($script)-2);
        }
        
        return $style;
    }
    
    public function src($path,$attrs="")
    {
        return "<img src='".URL."/img/".str_replace("/","|",$path)."' ".$attrs." />";
    }
}

class Image
{
    public function bg($path)
    {
        return "style=\"background-image:url('".CSS_PATH."/img/".$path."');\"";
    }
    
    public function src($path,$attrs="",$default_path=true)
    {
        $path = ($default_path ? IMG_PATH."/" : "").$path;
        return "<img src='".$path."' ".$attrs." />";
    }
}

class Youtube
{
    public function checkStr($str)
    {
        $regex = "#youtu(be.com|.b)(/v/|/watch\\?v=|e/|/watch(.+)v=)(.{11})#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[4][0];
    }

    public function embedUrl($code,$params=null)
    {
        $url = 'http://www.youtube.com/embed/'.$code;
        if($params) $url.= '?'.$params;
        return $url;
    }

    public function embed($code,$width=640,$height=480)
    {
        $url = 'http://www.youtube.com/embed/'.$code.'?autohide=1';
        return '<iframe '.
                'width="'.$width.'" '.
                'height="'.$height.'" '.
                'src="'.$url.'" '.
                'frameborder="0" '.
                'allowfullscreen></iframe>';
    }

    /**
     * @param string $n - 0,1,2,3 | default | mqdefault | maxresdefault
     */
    public function thumbnail($url=null,$n='mqdefault')
    {
        // $queryString = parse_url($url ? $url : self::getUrl(), PHP_URL_QUERY);
        // parse_str($queryString, $params);
        // return "i3.ytimg.com/vi/{$params['v']}/default.jpg";
        return 'http://i3.ytimg.com/vi/'.self::getCode($url).'/'.$n.'.jpg';
    }
    
    public function parseStr($str,$params=array())
    {
        $o = new stdClass();
        $o->type = 'youtube';
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        $o->embed = (isset($params['embed_width']) && isset($params['embed_height'])) ? 
                    self::embed($o->code,$params['embed_width'],$params['embed_height']) : 
                    self::embed($o->code);
        $o->embedUrl  = self::embedUrl($o->code);
        $o->thumbnail = self::thumbnail($o->url,(isset($params['thumbnail_youtube']) ? $params['thumbnail_youtube'] : 'mqdefault'));
        return $o;
    }
}

class Vimeo
{
    public function checkStr($str)
    {
        $regex = "#vimeo(.com|.b)(/)(\d{4,})#";
        preg_match_all($regex , $str, $m);
        return $m;
    }
    
    public function hasVideo($str)
    {
        $s = self::checkStr($str);
        return count($s[0]) ? $s : false;
    }
    
    public function getText($str)
    {
        if(!$s = self::hasVideo($str)) return $str;
        $p = '/(.*)(http.*)/';
        preg_match($p,$str,$m);
        return $m[1];
    }
    
    public function getUrl($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[0][0];
    }
    
    public function getCode($str)
    {
        if(!$s = self::hasVideo($str)) return null;
        return $s[3][0];
    }

    public function embedUrl($code,$params=null)
    {
        $url = 'http://player.vimeo.com/video/'.$code;
        if($params) $url.= '?'.$params;
        return $url;
    }

    public function embed($code,$width=640,$height=480)
    {
        $url = 'http://player.vimeo.com/video/'.$code.'';
        return '<iframe '.
                'width="'.$width.'" '.
                'height="'.$height.'" '.
                'src="'.$url.'" '.
                'frameborder="0" '.
                'allowfullscreen></iframe>';
    }

    public function thumbnail($url=null,$n='thumbnail_medium')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://vimeo.com/api/v2/video/'.self::getCode($url).'.php');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = unserialize(curl_exec($ch));
        $output = $output[0];
        curl_close($ch);
        return $output[$n];
    }
    
    public function parseStr($str)
    {
        $o = new stdClass();
        $o->type = 'vimeo';
        $o->text = self::getText($str);
        $o->url  = self::getUrl($str);
        $o->code = self::getCode($str);
        $o->embed = (isset($params['embed_width']) && isset($params['embed_height'])) ? 
                    self::embed($o->code,$params['embed_width'],$params['embed_height']) : 
                    self::embed($o->code);
        $o->embedUrl  = self::embedUrl($o->code);
        $o->thumbnail = self::thumbnail($o->url,(isset($params['thumbnail_vimeo']) ? $params['thumbnail_vimeo'] : 'thumbnail_medium'));
        return $o;
    }
}

function _d($var,$exit=true){ return Is_Var::dump($var,$exit); }
function _e($var,$exit=true){ return Is_Var::export($var,$exit); }

function export_url($section,$ext)
{
    $url = explode('/'.$section,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    return implode('/',array($url[0],$section,'export.'.$ext.$url[1]));
}
function export_url_report($section,$action,$ext)
{
    $url = explode('/'.$section.'/'.$action,'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    $qs  = (bool)trim($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'].'&' : '?';
    return implode('/',array($url[0],$section,$action)).$qs.'export='.$ext;
}

function _currency($v,$view=null)
{
    return $view ? $view->currency($v) : 'R$ '.number_format($v,2,',','.');
}

function _currencyParcel($v,$parc,$view=null)
{
    return $view ? $view->currency($v/$parc) : 'R$ '.number_format($v/$parc,2,',','.');
}

function _utfRow($row){ return Is_Array::utf8DbRow($row); }
function _utfRows($rows){ return Is_Array::utf8DbResult($rows); }

function cleanHtml($text){
    // $tags = '<b><strong><p><div><a><i><u><ul><ol><li><img><br><h1><h2><h3><h4><h5><h6><table><thead><tbody><th><tr><td><hr>';
    $tags = 'b,strong,p,div,a,i,u,ul,ol,li,img,br,h1,h2,h3,h4,h5,h6,'.
            'table,thead,tbody,th,tr,td,hr,strike';
    $tags = '<'.implode('><',explode(',',$tags)).'>';
    $text = stripslashes(($text));
    $text = (strip_tags($text,$tags));

    // replace contents simple text
    $replaces = array(
        '“' => '"',
        '”' => '"',
        "\xC2\xAB"     => "<<", 
        "\xC2\xBB"     => ">>",
        "\xE2\x80\x98" => "'",
        "\xE2\x80\x99" => "'",
        "\xE2\x80\x9A" => "'",
        "\xE2\x80\x9B" => "'",
        "\xE2\x80\x9C" => '"',
        "\xE2\x80\x9D" => '"',
        "\xE2\x80\x9E" => '"',
        "\xE2\x80\x9F" => '"',
        "\xE2\x80\xB9" => "<",
        "\xE2\x80\xBA" => ">",
        "\xE2\x80\x93" => "-",
        "\xE2\x80\x94" => "-",
        "\xE2\x80\xA6" => "...",
    );
    foreach($replaces as $p => $r) $text = str_replace($p,$r,$text);
    
    // replace contents
    $replaces = array(
        //'\<a(.*)\<\/a\>' => '<a target="_blank" rel="nofollow" \\1</a>',
        // '<([a-zA-Z0-9]*)(.*)(/?)(.*)>' => '<\\1 \\3>',
        // 'style\=\"(.*)\"' => ' ',
    );
    foreach($replaces as $p => $r) $text = ereg_replace($p,$r,$text);

    // import html
    Lib::import('phpQuery');
    $html = phpQuery::newDocumentHTML($text);

    // add attributes
    $adds = array(
        'a' => array('target'=>'_blank','rel'=>'nofollow')
    );
    foreach($adds as $p => $r) foreach($r as $k => $v) $html->find($p)->attr($k,$v);

    // clean styles
    $allow_styles = array(
        'text-align: center',
        'text-align: left',
        'text-align: right',
        'text-align: justify',
        'width: 100%'
    );
    
    foreach (explode('><',substr($tags,1,-1)) as $tag) {
        $items = $html->find($tag);
        
        foreach ($items as $item) {
            $style = array();

            foreach ($allow_styles as $as) {
                if(strstr(pq($item)->attr('style'), $as)) $style[] = $as;
            }

            (count($style)) ? 
                pq($item)->attr('style',implode(';',$style)) : 
                pq($item)->removeAttr('style');

            pq($item)->removeAttr('face');
            pq($item)->removeAttr('class');
        }
    }
    
    return $html->html();
    // return $text;
}

function pedidoDataHora($data)
{
    if(!(bool)trim(@$data)) return array('','','');
    
    $pedido_datahora = $data;
    $p_datahora = explode(' ',$pedido_datahora);
    $p_data = Is_Date::am2br($p_datahora[0]);
    $p_hora = substr($p_datahora[1],0,-3);
    $p_datahora = $p_data.' '.$p_hora;

    return array($p_data,$p_hora,$p_datahora);
}

function pedidoFormaEntrega($taxas)
{
    $forma = '';

    foreach($taxas as $taxa)
        if(strstr($taxa->descricao,'orreio'))
            $forma = trim(end(explode('-',$taxa->descricao)));

    return $forma;
}

function check_url_http($url,$pattern='://')
{
    return strstr($url, $pattern) ? $url : 'http://'.$url;
    // return strstr($url, 'http') ? $url : 'http://'.$url;
}

/**
 * Retorna URL da página para ser usado em templates, email, etc.
 * (não é usada a define URL para ser a mesma em ambos os ambientes)
 */
function site_url($withHttp=true)
{
    return ($withHttp ? (IS_SSL ? 'https://' : 'http://') : '').'www.'.SITE_NAME.'.com.br';
}

function site_link($anchor=null, $attrs='')
{
    return '<a href="'.site_url().'" '.$attrs.'>'.($anchor ? $anchor : site_url(false)).'</a>';
}

/**
 * Auxliares para emails
 */
function mail_rodape()
{
    return '<br>'.
           '<p>Em caso de dúvidas, por favor entre em contato através de nosso email <a href="mailto:contato@'.SITE_TITLE.'.com.br">contato@'.SITE_TITLE.'.com.br</a>.<br><br>'.
           // 'Horário de atendimento, de segunda à sexta das 08:00 às 18:00.</p><br>'.
           '</p><br>'.
           '<p>Atenciosamente,<br>'.SITE_TITLE.'<br>'.site_link().'</p>';
}

function _parseEndereco($dados)
{
    $endereco = $dados->endereco;
    $bairro = $dados->bairro;
    $cidade = $dados->cidade;
    $estado = $dados->estado;

    $endereco = reset(explode(' cj.', $endereco));
    $endereco = reset(explode(' cj ', $endereco));
    $endereco = reset(explode(' conj.', $endereco));
    $endereco = reset(explode(' conj ', $endereco));
    $endereco = reset(explode(' Cj.', $endereco));
    $endereco = reset(explode(' Cj ', $endereco));
    $endereco = reset(explode(' Conj.', $endereco));
    $endereco = reset(explode(' Conj ', $endereco));
    $endereco = reset(explode(' - ', $endereco));
    $bairro = reset(explode(' - ', $bairro));
    
    return implode(', ',array($endereco,$bairro,$cidade,$estado));
}

function posts_instagram($url,$cnt=2)
{
    if(!$fp=fopen($url ,"r" )) return array();

    $vHTML = "";
    while(!feof($fp)) $vHTML .= fgets($fp);
    fclose($fp);
    // _d($vHTML);
    $html = phpQuery::newDocumentHTML($vHTML);
    preg_match('/_sharedData = {(.*)};<\/script>/', $html->html(), $match);
    if(!(bool)$match) return array();
    // _d($match,0);

    $data = json_decode('{'.$match[1].'}');
    // _d($data,0);
    $media = $data->entry_data->ProfilePage[0]->user->media->nodes;
    // _d($media);

    return array_slice($media, 0, $cnt);
}

function posts_pinterest($url,$cnt=2)
{
    if(!$fp=fopen($url ,"r" )) return array();

    $vHTML = "";
    while(!feof($fp)) $vHTML .= fgets($fp);
    fclose($fp);
    // _d($vHTML);
    $html = phpQuery::newDocumentHTML($vHTML);
    $match = $html->find('.userBoards')->find('.GridItems')->find('.item');
    if(!(bool)$match) return array();
    $boards = array();
    foreach ($match as $m) {
        $match = pq($m);
        $link = $match->find('.boardLinkWrapper')->attr('href');
        $name = $match->find('.name')->html();
        $pins = $match->find('.PinCount')->html();
        $img = $match->find('.boardCover')->attr('src');
        
        $thumbs = array();
        $_thumbs = $match->find('.boardThumbs li');
        foreach ($_thumbs as $t)
            $thumbs[] = pq($t)->find('.thumb')->attr('src');
        // _d(htmlentities($_thumbs->html()));

        // _d(htmlentities($match->html()));
        $b = (object)array(
            'board_link' => 'http://pinterest.com'.$link,
            'board_name' => $name,
            'board_image' => $img,
            'board_pin_count' => strip_tags($pins),
            'board_thumbs' => $thumbs,
        );
        $boards[] = $b;
    }
    
    return array_slice($boards, 0, $cnt);
}

